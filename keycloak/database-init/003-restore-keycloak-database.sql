--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

\connect keycloak_dev

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO keycloak;

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO keycloak;

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO keycloak;

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO keycloak;

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO keycloak;

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO keycloak;

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO keycloak;

--
-- Name: client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO keycloak;

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    value character varying(4000),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_attributes OWNER TO keycloak;

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO keycloak;

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO keycloak;

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO keycloak;

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO keycloak;

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO keycloak;

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_client (
    client_id character varying(255) NOT NULL,
    scope_id character varying(255) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO keycloak;

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO keycloak;

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO keycloak;

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO keycloak;

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO keycloak;

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO keycloak;

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO keycloak;

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO keycloak;

--
-- Name: component; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO keycloak;

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO keycloak;

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO keycloak;

--
-- Name: credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO keycloak;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO keycloak;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO keycloak;

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO keycloak;

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO keycloak;

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO keycloak;

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO keycloak;

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO keycloak;

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO keycloak;

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO keycloak;

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO keycloak;

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO keycloak;

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO keycloak;

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO keycloak;

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO keycloak;

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO keycloak;

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO keycloak;

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO keycloak;

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO keycloak;

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO keycloak;

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO keycloak;

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO keycloak;

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO keycloak;

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO keycloak;

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO keycloak;

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO keycloak;

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO keycloak;

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO keycloak;

--
-- Name: realm; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL,
    default_role character varying(255)
);


ALTER TABLE public.realm OWNER TO keycloak;

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    value text
);


ALTER TABLE public.realm_attribute OWNER TO keycloak;

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO keycloak;

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO keycloak;

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO keycloak;

--
-- Name: realm_localizations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_localizations (
    realm_id character varying(255) NOT NULL,
    locale character varying(255) NOT NULL,
    texts text NOT NULL
);


ALTER TABLE public.realm_localizations OWNER TO keycloak;

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO keycloak;

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO keycloak;

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO keycloak;

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO keycloak;

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO keycloak;

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO keycloak;

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO keycloak;

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO keycloak;

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO keycloak;

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode character varying(15) NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO keycloak;

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO keycloak;

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy character varying(20),
    logic character varying(20),
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO keycloak;

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO keycloak;

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO keycloak;

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO keycloak;

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO keycloak;

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO keycloak;

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO keycloak;

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO keycloak;

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO keycloak;

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO keycloak;

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO keycloak;

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO keycloak;

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO keycloak;

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO keycloak;

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO keycloak;

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO keycloak;

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO keycloak;

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO keycloak;

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO keycloak;

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO keycloak;

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO keycloak;

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO keycloak;

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
45bd6c7b-6afa-408d-a290-7def1d767116	\N	auth-cookie	master	2d2f2420-e60b-4145-8e40-91d44beb6469	2	10	f	\N	\N
03f21565-5cde-48b9-a100-eeb244c97a19	\N	auth-spnego	master	2d2f2420-e60b-4145-8e40-91d44beb6469	3	20	f	\N	\N
8c4599d2-7cc9-443d-9f08-7880894c2c85	\N	identity-provider-redirector	master	2d2f2420-e60b-4145-8e40-91d44beb6469	2	25	f	\N	\N
2d608fb4-d670-4cb9-8479-b1fb0c343f6a	\N	\N	master	2d2f2420-e60b-4145-8e40-91d44beb6469	2	30	t	38ed2b6b-ed55-4d6b-9d05-4dbb52458e22	\N
6a86b804-e7f8-4eaa-8e08-4eed26bb13d5	\N	auth-username-password-form	master	38ed2b6b-ed55-4d6b-9d05-4dbb52458e22	0	10	f	\N	\N
cc43aa4f-3639-4bcc-8d16-08e13d0703eb	\N	\N	master	38ed2b6b-ed55-4d6b-9d05-4dbb52458e22	1	20	t	01a5f0a2-aa68-491f-8e20-7a3c3dc4f6c1	\N
522fcfb9-cd48-42b8-9755-a045faa313d6	\N	conditional-user-configured	master	01a5f0a2-aa68-491f-8e20-7a3c3dc4f6c1	0	10	f	\N	\N
009d0c54-69ce-4a6f-a4a9-61fbf7599b83	\N	auth-otp-form	master	01a5f0a2-aa68-491f-8e20-7a3c3dc4f6c1	0	20	f	\N	\N
b6f448bb-26af-4e64-8c24-ba8ab950a0f3	\N	direct-grant-validate-username	master	a7bddd0c-fb4c-4bba-bb0d-95bc2e4cf1e1	0	10	f	\N	\N
72fe79d9-db5f-4b01-ad83-135bf336152c	\N	direct-grant-validate-password	master	a7bddd0c-fb4c-4bba-bb0d-95bc2e4cf1e1	0	20	f	\N	\N
124ff951-c6f8-4ba9-9128-0ab8feb42cc1	\N	\N	master	a7bddd0c-fb4c-4bba-bb0d-95bc2e4cf1e1	1	30	t	365618be-8e72-46c6-abb1-e60a99efcc52	\N
bd507eb3-dc98-4fd3-9e12-d0efa4f6b569	\N	conditional-user-configured	master	365618be-8e72-46c6-abb1-e60a99efcc52	0	10	f	\N	\N
df098922-eeec-4d67-a2b5-c62fb1354d2e	\N	direct-grant-validate-otp	master	365618be-8e72-46c6-abb1-e60a99efcc52	0	20	f	\N	\N
29a71187-51ea-4529-8e7a-1310f1bbbbae	\N	registration-page-form	master	d5712b39-504b-43e9-84a8-aa03e5b95452	0	10	t	bafe4221-009d-415a-a2c9-e84ae60897fc	\N
ff0473b0-d430-442c-8991-51fd335ba4ea	\N	registration-user-creation	master	bafe4221-009d-415a-a2c9-e84ae60897fc	0	20	f	\N	\N
9c380875-59fa-40da-a1a1-ed23c89a5af8	\N	registration-profile-action	master	bafe4221-009d-415a-a2c9-e84ae60897fc	0	40	f	\N	\N
eb56632c-f346-4e08-b46a-ebc9cb1020ae	\N	registration-password-action	master	bafe4221-009d-415a-a2c9-e84ae60897fc	0	50	f	\N	\N
54e8ee7b-5aea-4f4b-9f49-0067bd1c270f	\N	registration-recaptcha-action	master	bafe4221-009d-415a-a2c9-e84ae60897fc	3	60	f	\N	\N
8101ecc2-cd78-4a8d-a4e0-881f73228929	\N	reset-credentials-choose-user	master	023b6442-7390-444a-b57e-f6f4618e2cb4	0	10	f	\N	\N
d0233b40-e8b3-4d97-a3d7-52ee7b3fd0d1	\N	reset-credential-email	master	023b6442-7390-444a-b57e-f6f4618e2cb4	0	20	f	\N	\N
2dd929e4-e4bd-4db7-99e1-b3cd8a4f3590	\N	reset-password	master	023b6442-7390-444a-b57e-f6f4618e2cb4	0	30	f	\N	\N
e2928b45-aa64-41e0-89d3-822d81b5b7a9	\N	\N	master	023b6442-7390-444a-b57e-f6f4618e2cb4	1	40	t	108f27f2-07f6-477f-822d-8d50731aed82	\N
66d03373-577a-4643-b3ea-9b247f369d2e	\N	conditional-user-configured	master	108f27f2-07f6-477f-822d-8d50731aed82	0	10	f	\N	\N
4af52657-1b8a-4456-8aef-71e465c20bbc	\N	reset-otp	master	108f27f2-07f6-477f-822d-8d50731aed82	0	20	f	\N	\N
8c7ea82a-c833-4b80-ac73-536d2ca19fbf	\N	client-secret	master	bf511b80-3e1b-4779-8f86-0186a12c5a05	2	10	f	\N	\N
fa5830f4-1afa-4db9-bbf6-cc9ee78be77a	\N	client-jwt	master	bf511b80-3e1b-4779-8f86-0186a12c5a05	2	20	f	\N	\N
62570609-d439-4c93-b5ec-48ef00e3746a	\N	client-secret-jwt	master	bf511b80-3e1b-4779-8f86-0186a12c5a05	2	30	f	\N	\N
f1df8227-60f1-4d75-ae20-0cb1cd3e7c86	\N	client-x509	master	bf511b80-3e1b-4779-8f86-0186a12c5a05	2	40	f	\N	\N
d808ae35-44ac-476a-b82f-60d8a7f49baa	\N	idp-review-profile	master	5c8c8da5-b430-4005-b35e-56970227ad0e	0	10	f	\N	a32b5e2f-d2a7-4a63-aec4-4d4e6dd04811
d3ad04c7-ddf2-4549-bce1-2cbec5ce0916	\N	\N	master	5c8c8da5-b430-4005-b35e-56970227ad0e	0	20	t	4e1aac00-2e8f-4949-867e-b2ceefc16c76	\N
77e03f2d-f327-4a27-85eb-d646153d5841	\N	idp-create-user-if-unique	master	4e1aac00-2e8f-4949-867e-b2ceefc16c76	2	10	f	\N	25d18666-743f-4150-b23e-b03928a8a762
6db7a143-25ed-4ccc-9865-be39a9efcca1	\N	\N	master	4e1aac00-2e8f-4949-867e-b2ceefc16c76	2	20	t	cebd1b7a-afa0-462f-9be6-67260ef7a524	\N
096faf97-eea2-475e-bb3f-88298561e42e	\N	idp-confirm-link	master	cebd1b7a-afa0-462f-9be6-67260ef7a524	0	10	f	\N	\N
df5a0ec5-75ac-4daa-bebb-6211f00781c8	\N	\N	master	cebd1b7a-afa0-462f-9be6-67260ef7a524	0	20	t	36602e3e-ff08-454a-a634-5a0a7adb507b	\N
bbdcac50-57f8-4cad-ba0f-b358e9731309	\N	idp-email-verification	master	36602e3e-ff08-454a-a634-5a0a7adb507b	2	10	f	\N	\N
8e41f354-6294-4b8d-b5c3-511827e56b38	\N	\N	master	36602e3e-ff08-454a-a634-5a0a7adb507b	2	20	t	846cc5fa-e6af-4fc2-8992-ee12246811e4	\N
66253415-e67d-49ba-809f-c456bf8f7df5	\N	idp-username-password-form	master	846cc5fa-e6af-4fc2-8992-ee12246811e4	0	10	f	\N	\N
6f84be4c-8638-48e2-901a-7663d540cefc	\N	\N	master	846cc5fa-e6af-4fc2-8992-ee12246811e4	1	20	t	bf513e25-0776-44f9-82c2-c49afc741886	\N
1a365814-11a6-4d0d-9f9f-4e6d15280cdc	\N	conditional-user-configured	master	bf513e25-0776-44f9-82c2-c49afc741886	0	10	f	\N	\N
c830a1e2-2c48-4705-b417-e70d56e5b8ae	\N	auth-otp-form	master	bf513e25-0776-44f9-82c2-c49afc741886	0	20	f	\N	\N
ad0d3c6f-7e0a-448f-ac26-a55b8eb9896e	\N	http-basic-authenticator	master	06d8002a-428d-4dd4-8a39-72e3e123250c	0	10	f	\N	\N
8604aa3d-da4f-4cb9-a2cc-f5b02efdb13a	\N	docker-http-basic-authenticator	master	14a5bf84-b55e-4298-a77b-38bafb56a1a5	0	10	f	\N	\N
f44ab274-5765-4edd-a3f7-a9a281e6b157	\N	no-cookie-redirect	master	e83fe629-5ed1-4797-b825-6bc609a18b80	0	10	f	\N	\N
1f4e072e-9943-4d88-aba2-f0fdac090d99	\N	\N	master	e83fe629-5ed1-4797-b825-6bc609a18b80	0	20	t	8412b4d4-cf41-4b46-a030-ccb828c71220	\N
d112f3c9-e94a-40ed-b630-cfba1da87443	\N	basic-auth	master	8412b4d4-cf41-4b46-a030-ccb828c71220	0	10	f	\N	\N
bd1cba37-26e4-4a4b-ad61-fa69efdd92f8	\N	basic-auth-otp	master	8412b4d4-cf41-4b46-a030-ccb828c71220	3	20	f	\N	\N
82b09b6e-91c3-49d7-906a-82b6cf2fcfc3	\N	auth-spnego	master	8412b4d4-cf41-4b46-a030-ccb828c71220	3	30	f	\N	\N
c1e1dd86-3e9c-47f2-95c1-d91bed160fb4	\N	auth-cookie	graphql-demo	54c69e5b-221d-4a8c-a055-0e9b317aa57b	2	10	f	\N	\N
0a0aac8e-731c-4c49-8765-22e5288c93c0	\N	auth-spnego	graphql-demo	54c69e5b-221d-4a8c-a055-0e9b317aa57b	3	20	f	\N	\N
32e118f7-87f1-48d6-b70b-874e522b875f	\N	identity-provider-redirector	graphql-demo	54c69e5b-221d-4a8c-a055-0e9b317aa57b	2	25	f	\N	\N
7ec3c453-f63a-4d07-af67-296b1a714e6a	\N	\N	graphql-demo	54c69e5b-221d-4a8c-a055-0e9b317aa57b	2	30	t	da600d53-259d-4ac1-bd20-cddb887f24d7	\N
e0b736aa-6e06-45d3-8898-f8dedcf7bdd3	\N	auth-username-password-form	graphql-demo	da600d53-259d-4ac1-bd20-cddb887f24d7	0	10	f	\N	\N
2268c6b2-a6d9-4292-b7b9-7786fa33fbc8	\N	\N	graphql-demo	da600d53-259d-4ac1-bd20-cddb887f24d7	1	20	t	440ac6cb-3aeb-4150-95b2-026585e67a35	\N
0b5cd3ea-259b-4ca0-921b-d58f0a6740c4	\N	conditional-user-configured	graphql-demo	440ac6cb-3aeb-4150-95b2-026585e67a35	0	10	f	\N	\N
64c67c08-98c1-4059-b35b-8c9f4b1e954f	\N	auth-otp-form	graphql-demo	440ac6cb-3aeb-4150-95b2-026585e67a35	0	20	f	\N	\N
a6d6f03a-91b2-4bde-894c-4f2a0719a5d2	\N	direct-grant-validate-username	graphql-demo	17e47e05-936d-4e9b-a94e-fd7d1dd44e3c	0	10	f	\N	\N
10016a47-e9e7-4396-b63a-e0f355f3ef10	\N	direct-grant-validate-password	graphql-demo	17e47e05-936d-4e9b-a94e-fd7d1dd44e3c	0	20	f	\N	\N
2abcc8b3-487a-486d-a0c6-9b7fd05d838f	\N	\N	graphql-demo	17e47e05-936d-4e9b-a94e-fd7d1dd44e3c	1	30	t	95b71a3b-cd27-48a8-9636-e6819f771504	\N
7a118dc0-91b7-4221-bf7f-8c5d68a2b359	\N	conditional-user-configured	graphql-demo	95b71a3b-cd27-48a8-9636-e6819f771504	0	10	f	\N	\N
d53e998b-0586-4b40-afd1-4343d5948f3d	\N	direct-grant-validate-otp	graphql-demo	95b71a3b-cd27-48a8-9636-e6819f771504	0	20	f	\N	\N
6dd7d918-690e-4c4e-ad2d-876abbf03240	\N	registration-page-form	graphql-demo	00460774-743c-4bf9-b0fd-dcf205cbd2fc	0	10	t	4402e39a-44ea-4e24-a081-3b8cf9959c1c	\N
f5b752f6-3e2a-4530-9872-be56945a398e	\N	registration-user-creation	graphql-demo	4402e39a-44ea-4e24-a081-3b8cf9959c1c	0	20	f	\N	\N
c57aef2b-7204-4a5c-a361-f09353e1f273	\N	registration-profile-action	graphql-demo	4402e39a-44ea-4e24-a081-3b8cf9959c1c	0	40	f	\N	\N
23126d21-df36-478d-a0bf-628fbbe4f97d	\N	registration-password-action	graphql-demo	4402e39a-44ea-4e24-a081-3b8cf9959c1c	0	50	f	\N	\N
96e530a0-1eb4-4224-91b9-256bdef4157e	\N	registration-recaptcha-action	graphql-demo	4402e39a-44ea-4e24-a081-3b8cf9959c1c	3	60	f	\N	\N
7c2beea5-f724-4a2e-b265-8404a8521d2c	\N	reset-credentials-choose-user	graphql-demo	35df30ca-39e9-4add-a493-716b7491753e	0	10	f	\N	\N
ff1c0d11-b61e-4f50-b124-45f099042eff	\N	reset-credential-email	graphql-demo	35df30ca-39e9-4add-a493-716b7491753e	0	20	f	\N	\N
85d94365-c75c-42d1-84a1-88e84d570cd7	\N	reset-password	graphql-demo	35df30ca-39e9-4add-a493-716b7491753e	0	30	f	\N	\N
24b6004d-f2a6-478c-8946-a5be302117a3	\N	\N	graphql-demo	35df30ca-39e9-4add-a493-716b7491753e	1	40	t	2499163d-dc1c-490f-8f8a-cca3594f076b	\N
cde7197b-ab46-4376-b56b-b5658f77bf61	\N	conditional-user-configured	graphql-demo	2499163d-dc1c-490f-8f8a-cca3594f076b	0	10	f	\N	\N
ac665a32-2c3b-4c03-9551-1e5af460a080	\N	reset-otp	graphql-demo	2499163d-dc1c-490f-8f8a-cca3594f076b	0	20	f	\N	\N
cc3fde14-3c72-4a63-b677-ba923970b940	\N	client-secret	graphql-demo	34472a09-8ea4-42c6-a179-ee09e80684ba	2	10	f	\N	\N
b3bffaa9-813a-4894-a759-025249aafbb6	\N	client-jwt	graphql-demo	34472a09-8ea4-42c6-a179-ee09e80684ba	2	20	f	\N	\N
199e94e2-f02f-4ba9-a614-37d15c4edc59	\N	client-secret-jwt	graphql-demo	34472a09-8ea4-42c6-a179-ee09e80684ba	2	30	f	\N	\N
c8faed2c-a45e-487f-a386-9bd8a6534e95	\N	client-x509	graphql-demo	34472a09-8ea4-42c6-a179-ee09e80684ba	2	40	f	\N	\N
84590961-63e2-415a-9374-5469d25656bf	\N	idp-review-profile	graphql-demo	e40516ea-f7ca-4b1f-b111-10f69341eaea	0	10	f	\N	8d07596c-05f2-47ba-a567-7fcc04be89de
4f0088b2-0e4b-42dd-9690-ef962a72d8ca	\N	\N	graphql-demo	e40516ea-f7ca-4b1f-b111-10f69341eaea	0	20	t	18b98146-a851-4532-a6e2-48c1a1228b26	\N
9ba600e4-506a-49ad-b278-ce474157078a	\N	idp-create-user-if-unique	graphql-demo	18b98146-a851-4532-a6e2-48c1a1228b26	2	10	f	\N	cdc17d76-4877-4832-b57a-faa263f9e7cf
df42a7cd-3f8e-4bb1-9e95-542ddd261e1a	\N	\N	graphql-demo	18b98146-a851-4532-a6e2-48c1a1228b26	2	20	t	644d576c-1d61-48c0-9e5f-7b2517d80b5c	\N
8d7838b2-5ab6-4dcb-b9d6-a56d2fc723cc	\N	idp-confirm-link	graphql-demo	644d576c-1d61-48c0-9e5f-7b2517d80b5c	0	10	f	\N	\N
9d9a41f0-5643-4caa-9367-f4ac7989e80e	\N	\N	graphql-demo	644d576c-1d61-48c0-9e5f-7b2517d80b5c	0	20	t	d2df9d89-ff81-4cf9-a7b1-3a0d2ab3d27f	\N
3bf83265-b907-47a1-a4dc-8ebae9d3d8f1	\N	idp-email-verification	graphql-demo	d2df9d89-ff81-4cf9-a7b1-3a0d2ab3d27f	2	10	f	\N	\N
c7c74676-29d6-4612-ae52-f0858e44ab14	\N	\N	graphql-demo	d2df9d89-ff81-4cf9-a7b1-3a0d2ab3d27f	2	20	t	573fee95-9af6-47c7-919a-9463463fdb6a	\N
d76c1b1c-a839-4ef4-a82e-f07c1935a4c4	\N	idp-username-password-form	graphql-demo	573fee95-9af6-47c7-919a-9463463fdb6a	0	10	f	\N	\N
fe593616-5f8b-4206-8397-44fd968f1260	\N	\N	graphql-demo	573fee95-9af6-47c7-919a-9463463fdb6a	1	20	t	e8a74a30-69d2-4687-b993-15c03577f540	\N
db8c75a4-ac01-4a0b-839e-2c252751246e	\N	conditional-user-configured	graphql-demo	e8a74a30-69d2-4687-b993-15c03577f540	0	10	f	\N	\N
d9cb4221-5178-4ced-8526-a7360e7d4f79	\N	auth-otp-form	graphql-demo	e8a74a30-69d2-4687-b993-15c03577f540	0	20	f	\N	\N
79213032-70e6-4872-bf5f-78235f78cfb5	\N	http-basic-authenticator	graphql-demo	c896588a-f520-4589-8d18-3fd5eb5b0ed5	0	10	f	\N	\N
66f5a6ba-e4cd-4eb7-b585-502f687d1311	\N	docker-http-basic-authenticator	graphql-demo	d9e99bec-4dbf-4fc8-aec5-29c0064fddac	0	10	f	\N	\N
a7813a79-4aa1-40dc-b374-cb704259ccb7	\N	no-cookie-redirect	graphql-demo	7ece8dae-5d1f-4fd4-9ca8-39c82034f772	0	10	f	\N	\N
9e5ea68f-1b39-4b54-89b8-9803d658ee89	\N	\N	graphql-demo	7ece8dae-5d1f-4fd4-9ca8-39c82034f772	0	20	t	9b082367-c156-42bc-b05d-b4083c66c081	\N
a24e2722-c946-4c16-ae00-58688ed85b9c	\N	basic-auth	graphql-demo	9b082367-c156-42bc-b05d-b4083c66c081	0	10	f	\N	\N
b853769d-9c52-41ba-b21c-61e0a20d7426	\N	basic-auth-otp	graphql-demo	9b082367-c156-42bc-b05d-b4083c66c081	3	20	f	\N	\N
835484bd-c95d-4ae1-b85f-07df9b61226c	\N	auth-spnego	graphql-demo	9b082367-c156-42bc-b05d-b4083c66c081	3	30	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
2d2f2420-e60b-4145-8e40-91d44beb6469	browser	browser based authentication	master	basic-flow	t	t
38ed2b6b-ed55-4d6b-9d05-4dbb52458e22	forms	Username, password, otp and other auth forms.	master	basic-flow	f	t
01a5f0a2-aa68-491f-8e20-7a3c3dc4f6c1	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
a7bddd0c-fb4c-4bba-bb0d-95bc2e4cf1e1	direct grant	OpenID Connect Resource Owner Grant	master	basic-flow	t	t
365618be-8e72-46c6-abb1-e60a99efcc52	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
d5712b39-504b-43e9-84a8-aa03e5b95452	registration	registration flow	master	basic-flow	t	t
bafe4221-009d-415a-a2c9-e84ae60897fc	registration form	registration form	master	form-flow	f	t
023b6442-7390-444a-b57e-f6f4618e2cb4	reset credentials	Reset credentials for a user if they forgot their password or something	master	basic-flow	t	t
108f27f2-07f6-477f-822d-8d50731aed82	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	master	basic-flow	f	t
bf511b80-3e1b-4779-8f86-0186a12c5a05	clients	Base authentication for clients	master	client-flow	t	t
5c8c8da5-b430-4005-b35e-56970227ad0e	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	master	basic-flow	t	t
4e1aac00-2e8f-4949-867e-b2ceefc16c76	User creation or linking	Flow for the existing/non-existing user alternatives	master	basic-flow	f	t
cebd1b7a-afa0-462f-9be6-67260ef7a524	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	master	basic-flow	f	t
36602e3e-ff08-454a-a634-5a0a7adb507b	Account verification options	Method with which to verity the existing account	master	basic-flow	f	t
846cc5fa-e6af-4fc2-8992-ee12246811e4	Verify Existing Account by Re-authentication	Reauthentication of existing account	master	basic-flow	f	t
bf513e25-0776-44f9-82c2-c49afc741886	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	master	basic-flow	f	t
06d8002a-428d-4dd4-8a39-72e3e123250c	saml ecp	SAML ECP Profile Authentication Flow	master	basic-flow	t	t
14a5bf84-b55e-4298-a77b-38bafb56a1a5	docker auth	Used by Docker clients to authenticate against the IDP	master	basic-flow	t	t
e83fe629-5ed1-4797-b825-6bc609a18b80	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	master	basic-flow	t	t
8412b4d4-cf41-4b46-a030-ccb828c71220	Authentication Options	Authentication options.	master	basic-flow	f	t
54c69e5b-221d-4a8c-a055-0e9b317aa57b	browser	browser based authentication	graphql-demo	basic-flow	t	t
da600d53-259d-4ac1-bd20-cddb887f24d7	forms	Username, password, otp and other auth forms.	graphql-demo	basic-flow	f	t
440ac6cb-3aeb-4150-95b2-026585e67a35	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	graphql-demo	basic-flow	f	t
17e47e05-936d-4e9b-a94e-fd7d1dd44e3c	direct grant	OpenID Connect Resource Owner Grant	graphql-demo	basic-flow	t	t
95b71a3b-cd27-48a8-9636-e6819f771504	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	graphql-demo	basic-flow	f	t
00460774-743c-4bf9-b0fd-dcf205cbd2fc	registration	registration flow	graphql-demo	basic-flow	t	t
4402e39a-44ea-4e24-a081-3b8cf9959c1c	registration form	registration form	graphql-demo	form-flow	f	t
35df30ca-39e9-4add-a493-716b7491753e	reset credentials	Reset credentials for a user if they forgot their password or something	graphql-demo	basic-flow	t	t
2499163d-dc1c-490f-8f8a-cca3594f076b	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	graphql-demo	basic-flow	f	t
34472a09-8ea4-42c6-a179-ee09e80684ba	clients	Base authentication for clients	graphql-demo	client-flow	t	t
e40516ea-f7ca-4b1f-b111-10f69341eaea	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	graphql-demo	basic-flow	t	t
18b98146-a851-4532-a6e2-48c1a1228b26	User creation or linking	Flow for the existing/non-existing user alternatives	graphql-demo	basic-flow	f	t
644d576c-1d61-48c0-9e5f-7b2517d80b5c	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	graphql-demo	basic-flow	f	t
d2df9d89-ff81-4cf9-a7b1-3a0d2ab3d27f	Account verification options	Method with which to verity the existing account	graphql-demo	basic-flow	f	t
573fee95-9af6-47c7-919a-9463463fdb6a	Verify Existing Account by Re-authentication	Reauthentication of existing account	graphql-demo	basic-flow	f	t
e8a74a30-69d2-4687-b993-15c03577f540	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	graphql-demo	basic-flow	f	t
c896588a-f520-4589-8d18-3fd5eb5b0ed5	saml ecp	SAML ECP Profile Authentication Flow	graphql-demo	basic-flow	t	t
d9e99bec-4dbf-4fc8-aec5-29c0064fddac	docker auth	Used by Docker clients to authenticate against the IDP	graphql-demo	basic-flow	t	t
7ece8dae-5d1f-4fd4-9ca8-39c82034f772	http challenge	An authentication flow based on challenge-response HTTP Authentication Schemes	graphql-demo	basic-flow	t	t
9b082367-c156-42bc-b05d-b4083c66c081	Authentication Options	Authentication options.	graphql-demo	basic-flow	f	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
a32b5e2f-d2a7-4a63-aec4-4d4e6dd04811	review profile config	master
25d18666-743f-4150-b23e-b03928a8a762	create unique user config	master
8d07596c-05f2-47ba-a567-7fcc04be89de	review profile config	graphql-demo
cdc17d76-4877-4832-b57a-faa263f9e7cf	create unique user config	graphql-demo
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
a32b5e2f-d2a7-4a63-aec4-4d4e6dd04811	missing	update.profile.on.first.login
25d18666-743f-4150-b23e-b03928a8a762	false	require.password.update.after.registration
8d07596c-05f2-47ba-a567-7fcc04be89de	missing	update.profile.on.first.login
cdc17d76-4877-4832-b57a-faa263f9e7cf	false	require.password.update.after.registration
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	f	master-realm	0	f	\N	\N	t	\N	f	master	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	f	account	0	t	\N	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
95089ef3-abe8-41ed-8139-2d06fe16544f	t	f	account-console	0	t	\N	/realms/master/account/	f	\N	f	master	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
1821be33-4f8a-43d5-8d4d-d538c440cf17	t	f	broker	0	f	\N	\N	t	\N	f	master	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
4866beda-e11d-43f5-b327-7be9abd38666	t	f	security-admin-console	0	t	\N	/admin/master/console/	f	\N	f	master	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
60af8b04-421e-442f-aaac-ff63258a6450	t	f	admin-cli	0	t	\N	\N	f	\N	f	master	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
f7e7b3b6-64d8-4236-9599-102c5a33323e	t	f	realm-management	0	f	\N	\N	t	\N	f	graphql-demo	openid-connect	0	f	f	${client_realm-management}	f	client-secret	\N	\N	\N	t	f	f	f
b369c12d-3d77-423a-891d-70a3164948cc	t	f	account-console	0	t	\N	/realms/graphql-demo/account/	f	\N	f	graphql-demo	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	t	f	broker	0	f	\N	\N	t	\N	f	graphql-demo	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	t	f	admin-cli	0	t	\N	\N	f	\N	f	graphql-demo	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
6d4ea0ce-66db-4183-939d-426781120ab2	t	f	graphql-demo-realm	0	f	\N	\N	t	\N	f	master	\N	0	f	f	graphql-demo Realm	f	client-secret	\N	\N	\N	t	f	f	f
30a27344-f11c-4837-926d-324134b7b6fe	t	f	security-admin-console	0	t	\N	/admin/graphql-demo/console/	f	\N	f	graphql-demo	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
316254a7-7597-4eab-bfca-6e492adce061	t	f	account	0	t	\N	/realms/graphql-demo/account/	f	\N	f	graphql-demo	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_attributes (client_id, value, name) FROM stdin;
95089ef3-abe8-41ed-8139-2d06fe16544f	S256	pkce.code.challenge.method
4866beda-e11d-43f5-b327-7be9abd38666	S256	pkce.code.challenge.method
b369c12d-3d77-423a-891d-70a3164948cc	S256	pkce.code.challenge.method
30a27344-f11c-4837-926d-324134b7b6fe	S256	pkce.code.challenge.method
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
9ba7abbf-450c-412b-93ec-f73cff93c4da	offline_access	master	OpenID Connect built-in scope: offline_access	openid-connect
f94ef230-1cfe-482f-a183-5ba6e41c7c9a	role_list	master	SAML role list	saml
d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	profile	master	OpenID Connect built-in scope: profile	openid-connect
f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	email	master	OpenID Connect built-in scope: email	openid-connect
b364bec7-e5a8-4450-9af5-64c0d643800e	address	master	OpenID Connect built-in scope: address	openid-connect
560030d5-6158-468d-be57-030f61f6b5aa	phone	master	OpenID Connect built-in scope: phone	openid-connect
4991299f-8091-4483-ab2d-355bb380a4f5	roles	master	OpenID Connect scope for add user roles to the access token	openid-connect
4e2b10ee-8d1b-4b83-bd89-01d03ca14950	web-origins	master	OpenID Connect scope for add allowed web origins to the access token	openid-connect
e960acdb-e83f-4604-8859-9e6fe8dfe0be	microprofile-jwt	master	Microprofile - JWT built-in scope	openid-connect
e4e2c8aa-74a2-49ba-97b8-c2857982cf01	offline_access	graphql-demo	OpenID Connect built-in scope: offline_access	openid-connect
cdba64eb-1c3f-43c8-a257-240eafa69541	role_list	graphql-demo	SAML role list	saml
5cf0e0b3-5da6-4111-98cd-e0c683c563c1	profile	graphql-demo	OpenID Connect built-in scope: profile	openid-connect
6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	email	graphql-demo	OpenID Connect built-in scope: email	openid-connect
d24b6228-f971-4fce-a41c-bc8db682db72	address	graphql-demo	OpenID Connect built-in scope: address	openid-connect
6a756304-fe3d-42bc-b2a8-68e9fa7c273f	phone	graphql-demo	OpenID Connect built-in scope: phone	openid-connect
f7050e4b-ef73-43ab-9274-3f6056949341	roles	graphql-demo	OpenID Connect scope for add user roles to the access token	openid-connect
80975af1-7043-4b09-9e0a-cea8849ced2e	web-origins	graphql-demo	OpenID Connect scope for add allowed web origins to the access token	openid-connect
19441e8b-6d47-417b-a8d8-794147a5204b	microprofile-jwt	graphql-demo	Microprofile - JWT built-in scope	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
9ba7abbf-450c-412b-93ec-f73cff93c4da	true	display.on.consent.screen
9ba7abbf-450c-412b-93ec-f73cff93c4da	${offlineAccessScopeConsentText}	consent.screen.text
f94ef230-1cfe-482f-a183-5ba6e41c7c9a	true	display.on.consent.screen
f94ef230-1cfe-482f-a183-5ba6e41c7c9a	${samlRoleListScopeConsentText}	consent.screen.text
d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	true	display.on.consent.screen
d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	${profileScopeConsentText}	consent.screen.text
d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	true	include.in.token.scope
f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	true	display.on.consent.screen
f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	${emailScopeConsentText}	consent.screen.text
f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	true	include.in.token.scope
b364bec7-e5a8-4450-9af5-64c0d643800e	true	display.on.consent.screen
b364bec7-e5a8-4450-9af5-64c0d643800e	${addressScopeConsentText}	consent.screen.text
b364bec7-e5a8-4450-9af5-64c0d643800e	true	include.in.token.scope
560030d5-6158-468d-be57-030f61f6b5aa	true	display.on.consent.screen
560030d5-6158-468d-be57-030f61f6b5aa	${phoneScopeConsentText}	consent.screen.text
560030d5-6158-468d-be57-030f61f6b5aa	true	include.in.token.scope
4991299f-8091-4483-ab2d-355bb380a4f5	true	display.on.consent.screen
4991299f-8091-4483-ab2d-355bb380a4f5	${rolesScopeConsentText}	consent.screen.text
4991299f-8091-4483-ab2d-355bb380a4f5	false	include.in.token.scope
4e2b10ee-8d1b-4b83-bd89-01d03ca14950	false	display.on.consent.screen
4e2b10ee-8d1b-4b83-bd89-01d03ca14950		consent.screen.text
4e2b10ee-8d1b-4b83-bd89-01d03ca14950	false	include.in.token.scope
e960acdb-e83f-4604-8859-9e6fe8dfe0be	false	display.on.consent.screen
e960acdb-e83f-4604-8859-9e6fe8dfe0be	true	include.in.token.scope
e4e2c8aa-74a2-49ba-97b8-c2857982cf01	true	display.on.consent.screen
e4e2c8aa-74a2-49ba-97b8-c2857982cf01	${offlineAccessScopeConsentText}	consent.screen.text
cdba64eb-1c3f-43c8-a257-240eafa69541	true	display.on.consent.screen
cdba64eb-1c3f-43c8-a257-240eafa69541	${samlRoleListScopeConsentText}	consent.screen.text
5cf0e0b3-5da6-4111-98cd-e0c683c563c1	true	display.on.consent.screen
5cf0e0b3-5da6-4111-98cd-e0c683c563c1	${profileScopeConsentText}	consent.screen.text
5cf0e0b3-5da6-4111-98cd-e0c683c563c1	true	include.in.token.scope
6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	true	display.on.consent.screen
6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	${emailScopeConsentText}	consent.screen.text
6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	true	include.in.token.scope
d24b6228-f971-4fce-a41c-bc8db682db72	true	display.on.consent.screen
d24b6228-f971-4fce-a41c-bc8db682db72	${addressScopeConsentText}	consent.screen.text
d24b6228-f971-4fce-a41c-bc8db682db72	true	include.in.token.scope
6a756304-fe3d-42bc-b2a8-68e9fa7c273f	true	display.on.consent.screen
6a756304-fe3d-42bc-b2a8-68e9fa7c273f	${phoneScopeConsentText}	consent.screen.text
6a756304-fe3d-42bc-b2a8-68e9fa7c273f	true	include.in.token.scope
f7050e4b-ef73-43ab-9274-3f6056949341	true	display.on.consent.screen
f7050e4b-ef73-43ab-9274-3f6056949341	${rolesScopeConsentText}	consent.screen.text
f7050e4b-ef73-43ab-9274-3f6056949341	false	include.in.token.scope
80975af1-7043-4b09-9e0a-cea8849ced2e	false	display.on.consent.screen
80975af1-7043-4b09-9e0a-cea8849ced2e		consent.screen.text
80975af1-7043-4b09-9e0a-cea8849ced2e	false	include.in.token.scope
19441e8b-6d47-417b-a8d8-794147a5204b	false	display.on.consent.screen
19441e8b-6d47-417b-a8d8-794147a5204b	true	include.in.token.scope
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	4991299f-8091-4483-ab2d-355bb380a4f5	t
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	b364bec7-e5a8-4450-9af5-64c0d643800e	f
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	560030d5-6158-468d-be57-030f61f6b5aa	f
95089ef3-abe8-41ed-8139-2d06fe16544f	4991299f-8091-4483-ab2d-355bb380a4f5	t
95089ef3-abe8-41ed-8139-2d06fe16544f	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
95089ef3-abe8-41ed-8139-2d06fe16544f	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
95089ef3-abe8-41ed-8139-2d06fe16544f	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
95089ef3-abe8-41ed-8139-2d06fe16544f	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
95089ef3-abe8-41ed-8139-2d06fe16544f	b364bec7-e5a8-4450-9af5-64c0d643800e	f
95089ef3-abe8-41ed-8139-2d06fe16544f	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
95089ef3-abe8-41ed-8139-2d06fe16544f	560030d5-6158-468d-be57-030f61f6b5aa	f
60af8b04-421e-442f-aaac-ff63258a6450	4991299f-8091-4483-ab2d-355bb380a4f5	t
60af8b04-421e-442f-aaac-ff63258a6450	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
60af8b04-421e-442f-aaac-ff63258a6450	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
60af8b04-421e-442f-aaac-ff63258a6450	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
60af8b04-421e-442f-aaac-ff63258a6450	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
60af8b04-421e-442f-aaac-ff63258a6450	b364bec7-e5a8-4450-9af5-64c0d643800e	f
60af8b04-421e-442f-aaac-ff63258a6450	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
60af8b04-421e-442f-aaac-ff63258a6450	560030d5-6158-468d-be57-030f61f6b5aa	f
1821be33-4f8a-43d5-8d4d-d538c440cf17	4991299f-8091-4483-ab2d-355bb380a4f5	t
1821be33-4f8a-43d5-8d4d-d538c440cf17	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
1821be33-4f8a-43d5-8d4d-d538c440cf17	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
1821be33-4f8a-43d5-8d4d-d538c440cf17	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
1821be33-4f8a-43d5-8d4d-d538c440cf17	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
1821be33-4f8a-43d5-8d4d-d538c440cf17	b364bec7-e5a8-4450-9af5-64c0d643800e	f
1821be33-4f8a-43d5-8d4d-d538c440cf17	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
1821be33-4f8a-43d5-8d4d-d538c440cf17	560030d5-6158-468d-be57-030f61f6b5aa	f
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	4991299f-8091-4483-ab2d-355bb380a4f5	t
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	b364bec7-e5a8-4450-9af5-64c0d643800e	f
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	560030d5-6158-468d-be57-030f61f6b5aa	f
4866beda-e11d-43f5-b327-7be9abd38666	4991299f-8091-4483-ab2d-355bb380a4f5	t
4866beda-e11d-43f5-b327-7be9abd38666	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
4866beda-e11d-43f5-b327-7be9abd38666	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
4866beda-e11d-43f5-b327-7be9abd38666	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
4866beda-e11d-43f5-b327-7be9abd38666	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
4866beda-e11d-43f5-b327-7be9abd38666	b364bec7-e5a8-4450-9af5-64c0d643800e	f
4866beda-e11d-43f5-b327-7be9abd38666	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
4866beda-e11d-43f5-b327-7be9abd38666	560030d5-6158-468d-be57-030f61f6b5aa	f
316254a7-7597-4eab-bfca-6e492adce061	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
316254a7-7597-4eab-bfca-6e492adce061	f7050e4b-ef73-43ab-9274-3f6056949341	t
316254a7-7597-4eab-bfca-6e492adce061	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
316254a7-7597-4eab-bfca-6e492adce061	80975af1-7043-4b09-9e0a-cea8849ced2e	t
316254a7-7597-4eab-bfca-6e492adce061	d24b6228-f971-4fce-a41c-bc8db682db72	f
316254a7-7597-4eab-bfca-6e492adce061	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
316254a7-7597-4eab-bfca-6e492adce061	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
316254a7-7597-4eab-bfca-6e492adce061	19441e8b-6d47-417b-a8d8-794147a5204b	f
b369c12d-3d77-423a-891d-70a3164948cc	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
b369c12d-3d77-423a-891d-70a3164948cc	f7050e4b-ef73-43ab-9274-3f6056949341	t
b369c12d-3d77-423a-891d-70a3164948cc	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
b369c12d-3d77-423a-891d-70a3164948cc	80975af1-7043-4b09-9e0a-cea8849ced2e	t
b369c12d-3d77-423a-891d-70a3164948cc	d24b6228-f971-4fce-a41c-bc8db682db72	f
b369c12d-3d77-423a-891d-70a3164948cc	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
b369c12d-3d77-423a-891d-70a3164948cc	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
b369c12d-3d77-423a-891d-70a3164948cc	19441e8b-6d47-417b-a8d8-794147a5204b	f
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	f7050e4b-ef73-43ab-9274-3f6056949341	t
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	80975af1-7043-4b09-9e0a-cea8849ced2e	t
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	d24b6228-f971-4fce-a41c-bc8db682db72	f
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
6b74f2e2-71f2-49a4-a9d7-21a3ffdc6d40	19441e8b-6d47-417b-a8d8-794147a5204b	f
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	f7050e4b-ef73-43ab-9274-3f6056949341	t
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	80975af1-7043-4b09-9e0a-cea8849ced2e	t
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	d24b6228-f971-4fce-a41c-bc8db682db72	f
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
583c55f0-6d14-45a7-98b0-b36bac7cf5e4	19441e8b-6d47-417b-a8d8-794147a5204b	f
f7e7b3b6-64d8-4236-9599-102c5a33323e	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
f7e7b3b6-64d8-4236-9599-102c5a33323e	f7050e4b-ef73-43ab-9274-3f6056949341	t
f7e7b3b6-64d8-4236-9599-102c5a33323e	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
f7e7b3b6-64d8-4236-9599-102c5a33323e	80975af1-7043-4b09-9e0a-cea8849ced2e	t
f7e7b3b6-64d8-4236-9599-102c5a33323e	d24b6228-f971-4fce-a41c-bc8db682db72	f
f7e7b3b6-64d8-4236-9599-102c5a33323e	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
f7e7b3b6-64d8-4236-9599-102c5a33323e	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
f7e7b3b6-64d8-4236-9599-102c5a33323e	19441e8b-6d47-417b-a8d8-794147a5204b	f
30a27344-f11c-4837-926d-324134b7b6fe	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
30a27344-f11c-4837-926d-324134b7b6fe	f7050e4b-ef73-43ab-9274-3f6056949341	t
30a27344-f11c-4837-926d-324134b7b6fe	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
30a27344-f11c-4837-926d-324134b7b6fe	80975af1-7043-4b09-9e0a-cea8849ced2e	t
30a27344-f11c-4837-926d-324134b7b6fe	d24b6228-f971-4fce-a41c-bc8db682db72	f
30a27344-f11c-4837-926d-324134b7b6fe	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
30a27344-f11c-4837-926d-324134b7b6fe	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
30a27344-f11c-4837-926d-324134b7b6fe	19441e8b-6d47-417b-a8d8-794147a5204b	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
9ba7abbf-450c-412b-93ec-f73cff93c4da	18ae6e54-993e-492d-8ba5-20545debc97d
e4e2c8aa-74a2-49ba-97b8-c2857982cf01	7953c80e-3bc1-4556-a678-704a4d07cb65
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
193827dd-de3c-4cf9-9a59-13b20db7b1e0	Trusted Hosts	master	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
5aa60dda-35cd-439e-8d5c-0bff45bdc49c	Consent Required	master	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
216e0f02-5856-45ca-a936-66cd9f893575	Full Scope Disabled	master	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
d4d4464e-a057-4b30-90cd-2b9e6bea5087	Max Clients Limit	master	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
667d646a-3663-45f1-9adf-bcb77a4950ea	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
f0f15451-1d70-4038-b531-694fa9b7adfc	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	anonymous
bce158e4-e2dd-41e8-b625-77ea160f83ab	Allowed Protocol Mapper Types	master	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
18b00d04-45c9-4db7-abef-aea70cedf847	Allowed Client Scopes	master	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	master	authenticated
26878a15-4b1d-41cb-8655-041d99e6828a	rsa-generated	master	rsa-generated	org.keycloak.keys.KeyProvider	master	\N
501ef341-453f-4b43-9c26-2d6b38b57c6f	hmac-generated	master	hmac-generated	org.keycloak.keys.KeyProvider	master	\N
96e70115-17da-438e-a449-f7be68c4077b	aes-generated	master	aes-generated	org.keycloak.keys.KeyProvider	master	\N
3f05f4a3-c439-4810-a64a-eaaa1e03d676	rsa-generated	graphql-demo	rsa-generated	org.keycloak.keys.KeyProvider	graphql-demo	\N
110bf5bd-6fbd-4e7f-9ea5-dd1b5d8a6bfa	hmac-generated	graphql-demo	hmac-generated	org.keycloak.keys.KeyProvider	graphql-demo	\N
77e5ef2f-8ca3-4b10-be00-fe4b281b26b5	aes-generated	graphql-demo	aes-generated	org.keycloak.keys.KeyProvider	graphql-demo	\N
1d172f8c-3975-4ce1-bed8-eafa5c5523d3	Trusted Hosts	graphql-demo	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
446bc055-8366-4397-a3f5-10f5552fcf79	Consent Required	graphql-demo	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
6bebec5c-6762-4df1-bd6c-2f675a9254e1	Full Scope Disabled	graphql-demo	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
08b8f5a0-2033-4999-90e2-e62fb839e88e	Max Clients Limit	graphql-demo	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	Allowed Protocol Mapper Types	graphql-demo	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
f71cfb5d-46fa-4c63-b9f8-97dc1ec02a0c	Allowed Client Scopes	graphql-demo	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	anonymous
a1f539a1-a5a0-44fc-9170-6e9da8c8192a	Allowed Protocol Mapper Types	graphql-demo	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	authenticated
890fc2ad-b181-4abe-a3bb-83be4ca7f42f	Allowed Client Scopes	graphql-demo	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	graphql-demo	authenticated
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
2824d0ab-6d88-4060-808b-9ec737268965	f0f15451-1d70-4038-b531-694fa9b7adfc	allow-default-scopes	true
64c1096e-5b09-4b33-bab1-9f6ed81854db	d4d4464e-a057-4b30-90cd-2b9e6bea5087	max-clients	200
ceb0e73a-e1d4-439b-8a45-508974f8d730	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	saml-user-property-mapper
a6954e05-e389-4dce-b568-4d10d4d2dcc9	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	oidc-address-mapper
a29677f3-93e7-43cc-ba21-c8b6282f4d76	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
9f0964cb-fd29-4f3e-8fd2-f29921ea51cb	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	oidc-full-name-mapper
110709d8-36c7-4b6b-a76e-d2712eefac42	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	saml-role-list-mapper
bf55ed84-1a20-43c0-b360-5146fc524434	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
1e536207-9c0c-4b9c-a5b4-d2258231047e	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	saml-user-attribute-mapper
a0339b1c-ab67-4ebd-9a9f-37fcf537e0d3	667d646a-3663-45f1-9adf-bcb77a4950ea	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
58b12bb9-8d55-435f-84c1-4b40f924bd6b	18b00d04-45c9-4db7-abef-aea70cedf847	allow-default-scopes	true
4224f3d7-5d46-425b-9246-466874dd47d9	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	saml-user-property-mapper
063f99dc-1859-40cf-8881-b9de1e1be313	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	oidc-address-mapper
4836f91e-7465-4fec-b328-f9eb0425e4c7	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
a1fa8d3c-6316-4b84-86cd-804ac9d24608	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	saml-user-attribute-mapper
664d8512-fa8a-4b4d-bf38-fa2a408bbbec	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
babb595c-448b-484e-84ab-f649de54eec8	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	oidc-full-name-mapper
150d7be7-a623-4ddc-aefd-f7fa970edaef	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
1edb390d-d598-4274-9a1b-bcf0dc1d7448	bce158e4-e2dd-41e8-b625-77ea160f83ab	allowed-protocol-mapper-types	saml-role-list-mapper
3e3e9a6f-c718-4275-9811-cd1fb91ded2f	193827dd-de3c-4cf9-9a59-13b20db7b1e0	client-uris-must-match	true
53357c5b-2e3c-4da3-86d5-4d347543747f	193827dd-de3c-4cf9-9a59-13b20db7b1e0	host-sending-registration-request-must-match	true
fd224f0f-8c01-4532-b0c9-48bfc17a91fe	501ef341-453f-4b43-9c26-2d6b38b57c6f	priority	100
8a90ae20-b243-46a7-a1d7-c6982e51cd31	501ef341-453f-4b43-9c26-2d6b38b57c6f	kid	eb14208c-2809-4fba-ba49-edfccd713ae2
64a9e845-c4d9-47c2-aa70-3172f0588f82	501ef341-453f-4b43-9c26-2d6b38b57c6f	secret	vQJJ1tnOEx_zfoKbQaPrbySPgW3miCXLowU7SOcRPfRZWrVApxGAz3gdHnykO0apayL2qOlpeUzrUfgfMvNMmg
0606aa7c-08b8-4928-a92a-6e34d3d08839	501ef341-453f-4b43-9c26-2d6b38b57c6f	algorithm	HS256
e7363362-19bb-4ef2-850c-1a721bfa4820	96e70115-17da-438e-a449-f7be68c4077b	priority	100
ff2609fe-8e11-454a-a36b-4d0ec03b45b7	96e70115-17da-438e-a449-f7be68c4077b	kid	82eb235a-a8bc-4872-8e0e-707a8f8102ce
b3b8638d-7cf7-4223-8a24-d1a3bdc9ef84	96e70115-17da-438e-a449-f7be68c4077b	secret	5eWBbmCz9Ifo-idMtmqpXg
95b74b20-940f-4a9b-9e60-14008734d233	26878a15-4b1d-41cb-8655-041d99e6828a	certificate	MIICmzCCAYMCBgF6KXc13zANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjEwNjIwMTI0NjU2WhcNMzEwNjIwMTI0ODM2WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCiJ3sgbE0AOb83/1b4FVSZbf0osAEsRplbhPLLwP6q3X9Khp6U/2jT9MY3jUZfl9j/JiHYzh5SQVhTnzg0Uuo4e70jhdtR9lvXRY/Pmd8z1qeKz1vrfUak1c3V3zhdEYhrby81NIV+qcMkbmVXpDV7rnud6voF3e1spgCtsm1KQ13UsRtNLg9PRWD53AkOzwYs3nAp6GaxK2UPEDvo8Asd7CK3c22mtO/PbOzQfDmgn+FnKr+KLIoBb018+XKsqeTjIvnjp5596rcBMEkJjBNNhZ+AYGfqjomww7KjiacKDHggcwvAW2goQZhD3KTnRS1Qzg7r6cBS1PeVFFhJykaVAgMBAAEwDQYJKoZIhvcNAQELBQADggEBADedUYX9xpwfPvI9K18AOhEBu2FtHB+9Od1R66UPJg/hRDnqgTu7ZRY/iEQAIKwow+i9keMNZSTl3OteUxtuQh8BmJdXjhsY5u6BZXXevMSFAF0ZKwF1BkpaUvaP6HABjPv9zHWSUpxNu+NJSNr1sq0OqrZ3WbAiN2jM7iKNtMd1jO4KAMftZQ5FUX2HA0O2uwLo1iwcx413r7Bqo2eU2fBFTIfdepkZ7Ohb/SbIxmtulJXdWgyx1Q0qfqfLJn7lqPwOpmEI4AUGv2P09RtfX7ltPRuUvWxtcBa/ZcMdmPLhtkGbfC13IeEU+o3UeWIfgWGCEFPCmsYIuQviOp+rmxM=
5b7fd390-0381-4bec-9579-2b8f2fbd1375	26878a15-4b1d-41cb-8655-041d99e6828a	priority	100
040c26cf-d364-416f-8842-44162c6fc924	26878a15-4b1d-41cb-8655-041d99e6828a	privateKey	MIIEowIBAAKCAQEAoid7IGxNADm/N/9W+BVUmW39KLABLEaZW4Tyy8D+qt1/SoaelP9o0/TGN41GX5fY/yYh2M4eUkFYU584NFLqOHu9I4XbUfZb10WPz5nfM9anis9b631GpNXN1d84XRGIa28vNTSFfqnDJG5lV6Q1e657ner6Bd3tbKYArbJtSkNd1LEbTS4PT0Vg+dwJDs8GLN5wKehmsStlDxA76PALHewit3NtprTvz2zs0Hw5oJ/hZyq/iiyKAW9NfPlyrKnk4yL546eefeq3ATBJCYwTTYWfgGBn6o6JsMOyo4mnCgx4IHMLwFtoKEGYQ9yk50UtUM4O6+nAUtT3lRRYScpGlQIDAQABAoIBAHX9MmuCQnlCi7AsXNUBeGB4FvivMPtNl7NvRtgZ43Ti3QeWcvfJ7TYlasHrS2StchkeXwyJ1qhVofae6V+xYXqeFx8s/ExkbalzlVQjC7WFoL2xaposuuWgKTlRRor6AgDik0AsqmJjFnqA1b7y3nPTn2FmJG12cZdYJzA8n6ejtuW22R6fWCIyI0Ljwnm7jiUOQXNwwKnuRnCZpKIaYvgdhRi484qen6EV6PvzkDAzBEVt1aW0oBiGOvEB5Cze8f7HTM4d1vZE3KNoWhzcjXOGgEjjLScJF3xU0ikhXWg3/IHBKuZY0AzzPQ6P0r2oXWmoV8EhchGKFT431rj2gLUCgYEA7yZ9Bw62IuG42BEGTUsCqKDAnxRIE7HgnOFsq74Xw/pjhGnxBTDBRYj7FFpmT3lYsTEZsy9QPYqGc0Sb9ogSy0/Xd+M5S7UszWdPNU+P8AnW6SWu/4rSt/yBQfpqbZpOPOycjSVLqDogb99PMUWvCq22ByLDYJLo+Cuj/Scdvx8CgYEArZQ6N0qX0XL5kvV6K0oZDuR7iNQI6N3q3+QSHLN8LaLxdf2Ylq84aocXvNNn55ExvqF/SsULegp3pdLrqheBxw9jTXLix0FLUgeqy4QNDpyptyWOANgqajYFeX3nbaa1GIefj+vUaU11fS5wuV0EHxgHQewc4bj+8vjIql4qJ8sCgYEA3K+avrF/uA6uIY0MULVAndXCCsqPfMUvrfWD77Q6gBoGTcHprU2BfKZcdDq7oOksxE64FZc6p5+2VJZiigyls0JZZoOjcs6cqmuEN4DVJcAetwWUSLKVozz7FqR8I+o5+pSqHsHk4DfLiRD8Jk7UqbkFee5Fd0/6dlDnQowfMicCgYBonkpoQOE4MCAUXzf/F2369+MtGP81u2tw/jmcauGRcAAwqWRqjblnG31HJfQeMfjVeOkL26+LIvvySioYHortu2ySpYfhqI0JJ+3dM/3arLnt1Qi9oYwO7/hkvWnbQPstibC2QeGIhlMl2/O9BT8SrpzRMspFkga30xK+/HpT+QKBgGd6ODeCRMPb5C9dQFqT9AGNa7PqJO3DM137WpMAAbfB/t6X1PALqhx8lfYTorh86JdfStwpgqzijgGtqeafOtSgrcnWNEZj1dh6I81UKlg2NPYXfJTbDIyH8bPVjFJ2gEP8+v+n/wy5bE9CTkZL8U2YLwwqFgDKQKfq5fF90osy
a3833881-9705-46b5-9969-e49151eb4994	77e5ef2f-8ca3-4b10-be00-fe4b281b26b5	priority	100
658b7488-fc4a-446c-98fd-e68d6945b98c	77e5ef2f-8ca3-4b10-be00-fe4b281b26b5	secret	0QbFQ1UF_-haQvT8L7OTOA
7d707941-831b-46b2-bfcb-d44aa0fd9c89	77e5ef2f-8ca3-4b10-be00-fe4b281b26b5	kid	dd737a77-4408-4634-8989-a506ab159e09
668efbdd-4a04-4431-87bd-a0b38835ceeb	3f05f4a3-c439-4810-a64a-eaaa1e03d676	privateKey	MIIEpQIBAAKCAQEApEjL23MFjGuPKyHUjJhZ+ZHBDvy5U/0hOLRiqY5DtLNkqkk+SVR2zR5bvEGlffw/Mp91Agg4VaqdY09YcptwRXICi6gpO/gtb0CciJ3LH7NgbDHyPF2Va1xCXPxw9NKu3ApaBj/aivaokMoLiJKG4A0L604AIqkbALZePJSPB0UBuh8VDBYfpUVrJ77FIC0n2HFI2JIcPxl+yWkzoT+MO404NHp7nmVeMOA6U+s6eIGrGmptM47m0RKq32v9BvjGFvqEAdXHg/f/FQIB0y8yvB3cR6UZvWn3MQkAC6T8JuG83BNWcLHtYHFXHOwx0WLmtSfHnBe2Fi6wHKvnXNGOcwIDAQABAoIBADbAlECVOxorltGXOtGIzSPiMeeezt1xFatiYyeDZIlK6JfgRUQHRHjkT0qUzmUItr9WaZa9ypiyfjXk8eNwbqCWeMa2ytdTxUC41fKqNFJiZBZrJwJ7Zhsb6no3ICZYOTLYuFww19NMla17yRBBVXZCouBPZLgoh/lazll+YZTYHvD9j/+wTlaT/mY0B9t+UZMIU754QR2Pt6mTaY70cVuG6J5WCPOfVOagMreRlsQwC7oZwmy/dmtHF7LFCpa/8zxW+poUIPlOjzHDf9Um6OSQS4yNpRyvcaKEwtHfFB+YFCnaQxgoyEUYrPtmKjhll48Ruwr9TRwZjz4p9l22dQECgYEA6gi83Gry5jplhRpa4VcNc2wOcfmE89e+0sjOgt1imXJCAJdxrOurYxWhlwn8o7IXEgbKsSRDoFs/uGCMATr5PP4UN2CX9ujmw+hK3si7UoA9SnRjlBANhqI0XvZOFeg/MsiNTv4VaQBCm0o34htHt3j14WNhe7F/SlknAeU96rMCgYEAs7Qk0UNYpWxw9slz8jsdoSOUSnktLuYpIGVFZgfXmaPm9UK1UZLd4vBtqxjvwdDEkNjc+uA5xkaYkUl9reGUwvhKYbbkd/VMJ/j/CQ972GkmxjrB+7L6/cp1xVzb/3WaFHkiuLpFr7I9GiYqIXSdMHJCJIT8LNPzvGe7sgSKrUECgYEAwIvFzlbTVuWOpq6jPKghcefiDpVqdC/5WHRLq1E3EOX3NYNjzA/s5s1iL+DQkWExJ+yckJc0aFcKmsgcB9scObkUqub2v/an/WA9UQO3Uk4dnwcY8jUu26jSC5r7bXyxmjd0Qxw6QzQqmJa8WXN2UpXbPb9E8b576pBkzmNryI8CgYEAgj4/LPgdqI+G9CagBWE5IVATK0LWL5abGiKsAL4QUgeL3hiCJ+RFMzTEl2xUYOm5fa3fw2h+FymVbD+SelLzqbeg2V9gCV1c29/IoD17qibT9nohnBZleUmiv++Ed4s5qDr/ollHJocNaAVfJa5KOxGkxKkpyCe6EMQyMly+UcECgYEAwXoqwuM8hjCe4Ns1IK1CALNWP3AGfMaDGQYEP1JcfUG2fEIfwDYjxgmbSjLtlc3jcLL6Xw3AuO4X1/MfXcTN5EDYty3qY+mblMRkNBvBaSQmPS2WlpbDlfUf0FlarX9lxowZs79Y9ZJYdYI8cr3TPmOC/lsQ+itrq1Za9v9O+3g=
bb408a51-49fc-47be-80e3-826d466ba747	3f05f4a3-c439-4810-a64a-eaaa1e03d676	certificate	MIICpzCCAY8CBgF6KXzuCzANBgkqhkiG9w0BAQsFADAXMRUwEwYDVQQDDAxncmFwaHFsLWRlbW8wHhcNMjEwNjIwMTI1MzExWhcNMzEwNjIwMTI1NDUxWjAXMRUwEwYDVQQDDAxncmFwaHFsLWRlbW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCkSMvbcwWMa48rIdSMmFn5kcEO/LlT/SE4tGKpjkO0s2SqST5JVHbNHlu8QaV9/D8yn3UCCDhVqp1jT1hym3BFcgKLqCk7+C1vQJyIncsfs2BsMfI8XZVrXEJc/HD00q7cCloGP9qK9qiQyguIkobgDQvrTgAiqRsAtl48lI8HRQG6HxUMFh+lRWsnvsUgLSfYcUjYkhw/GX7JaTOhP4w7jTg0enueZV4w4DpT6zp4gasaam0zjubREqrfa/0G+MYW+oQB1ceD9/8VAgHTLzK8HdxHpRm9afcxCQALpPwm4bzcE1Zwse1gcVcc7DHRYua1J8ecF7YWLrAcq+dc0Y5zAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAJTlenLwJj0syogQV0FKdRgQzUrKHAkvfX9sVJKgrVT1h3vCMmkjmvgv0vyLt4aaoYeFEs9dAS3JSrDgKE2ZoZCNilvAE2cnzQIoR7WiHfPXjRNMhPFIVoQpPpVtsDdcEv9tKLM9uq2hQQywkjb86ZStjnCqjxDlKZlm7JZijdwuZBsXD7vOHozSTrfYNJ0Qs+4M3S9T64YAxgYkINOYjzOruVJClDqCxvv+nAKnn8OkvXTUwVvUoHzp65Rnje/fsdq+Ta3bcmqhGyotLeS+9H2RAjgODpCjI+tAxw08psTxtV+Zy/1sW5nvj0UGB8BtV2FqiGJTyZ9ZBSzHEnWt+ss=
28bcb7b2-48a0-47f4-a8a3-c8dd90423443	3f05f4a3-c439-4810-a64a-eaaa1e03d676	priority	100
878be788-f95c-44fb-9a75-55b1356b4a04	110bf5bd-6fbd-4e7f-9ea5-dd1b5d8a6bfa	kid	1d48af62-99ed-4d1d-bc0a-e986dd2c9ea6
497081d4-8822-4468-9312-aa74269c6537	110bf5bd-6fbd-4e7f-9ea5-dd1b5d8a6bfa	priority	100
d52d661f-0801-4532-913f-a7b3b1d2c3c7	110bf5bd-6fbd-4e7f-9ea5-dd1b5d8a6bfa	algorithm	HS256
70074068-9d7c-4414-9cfd-22d7b45abc56	110bf5bd-6fbd-4e7f-9ea5-dd1b5d8a6bfa	secret	8h-cTM6MVUcBVye2G6qh-TrdCo8spl497EJJNsEY0DGYg0MRgMSbGiLvJeiD5QKCbK9FHq_PuQHntQlYgvFCpg
73dca27d-af50-4646-beb2-3bf226626ee1	08b8f5a0-2033-4999-90e2-e62fb839e88e	max-clients	200
dfcccadf-3c41-4590-9dc7-69016d821139	f71cfb5d-46fa-4c63-b9f8-97dc1ec02a0c	allow-default-scopes	true
af03d7c2-354f-41ec-a0f8-2b4a51d95e2f	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
e08203a4-a66b-4b47-9c3a-23b65e1ff37a	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	saml-role-list-mapper
ae9b6de4-b0e0-470e-92b9-7dbd2ef16a11	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	saml-user-property-mapper
80857811-8d6d-47d1-a609-8687f18746a0	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	oidc-address-mapper
16b54d3e-e553-4eee-afe7-28c66350f53e	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	saml-user-attribute-mapper
c3522954-e291-4afd-8c95-03a4e51a8e5b	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
30fb976e-024a-4815-98a4-344355b1765a	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
dc3a1a70-c4b4-4227-8cab-1d3e77e74fe6	a1f539a1-a5a0-44fc-9170-6e9da8c8192a	allowed-protocol-mapper-types	oidc-full-name-mapper
693f6b71-61eb-4fcf-ba41-d41a83c2b0ef	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
ca3f9abb-6af0-42bd-9bcd-42804c3ccdf7	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	saml-user-property-mapper
63b02a22-fe59-425d-b0ec-a44810fac65a	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
3d72b705-721b-4e76-b4a7-8218888630fa	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	saml-role-list-mapper
addffa96-562b-4f46-aa03-97ef6faf20bc	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	oidc-full-name-mapper
a34a77a9-f1b5-4dad-91de-938aec080c37	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
9b335d63-6ac9-49e6-a0e5-b744a204bfde	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	saml-user-attribute-mapper
319873be-962a-4f0f-8fc0-89d3a6123131	c3b7bcee-50d0-4d6c-85e4-fc81edba6bdd	allowed-protocol-mapper-types	oidc-address-mapper
f929becd-c2bc-4c1a-b634-318eb1dff52f	890fc2ad-b181-4abe-a3bb-83be4ca7f42f	allow-default-scopes	true
047a7905-40d1-45f8-8728-621e1bce9bbd	1d172f8c-3975-4ce1-bed8-eafa5c5523d3	client-uris-must-match	true
6a870096-83ce-476f-bbf1-003d9ebb777b	1d172f8c-3975-4ce1-bed8-eafa5c5523d3	host-sending-registration-request-must-match	true
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.composite_role (composite, child_role) FROM stdin;
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	b31d5b71-50e2-494f-baf3-c2badf61b943
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	cdc88a97-8096-42d4-aea7-05361497ff1e
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	4d18d93b-8591-4eb8-9bf5-fea7b3721955
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	4334fa82-2c49-47e2-b651-8c4f69ee9c5d
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	bc90ab75-f5d3-452c-94fa-0221a6250b34
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	bcffea9d-d1ab-4494-abed-1f10a54bbd72
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	26984bdd-5ec5-40bc-80d0-6d1596a238e2
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	738f5c98-fbac-4f5b-9531-56eb963231ad
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	9805cc37-3954-4fff-8eba-135492d6d220
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	f23c7da3-5ba3-4016-acef-324b2a0875c9
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	67d30fd4-e228-481d-b054-2bcdb7d0f43c
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	2f3ffbbf-ffb8-49b7-8f83-866df9beaab9
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	a2594d9c-2050-4b8c-8479-3246b208c897
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	7c951be2-4410-4808-80dd-c1b7471c38a0
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	9ee634bc-44cf-4091-886a-7e5044067b92
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	279b68de-ed6b-4d42-9232-c0549a4aab0c
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	6da80ff4-413d-48d3-acee-9268ff8bcef7
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	13167db1-6986-421d-99ba-eb609672867e
bc90ab75-f5d3-452c-94fa-0221a6250b34	279b68de-ed6b-4d42-9232-c0549a4aab0c
4334fa82-2c49-47e2-b651-8c4f69ee9c5d	9ee634bc-44cf-4091-886a-7e5044067b92
4334fa82-2c49-47e2-b651-8c4f69ee9c5d	13167db1-6986-421d-99ba-eb609672867e
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	f2e59bb4-0e83-4d94-9b2b-56addac4af12
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	81219ff6-ecd7-49f4-883d-d272f6bd7e2d
81219ff6-ecd7-49f4-883d-d272f6bd7e2d	27a33857-e363-41af-86be-06375d5182eb
167814db-6b10-4daf-8e07-adb8c75e97d5	8ecde4a4-f358-40ad-ba0c-b7f5f28f7965
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	81e8710e-9373-4851-8818-05b56a26d97f
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	18ae6e54-993e-492d-8ba5-20545debc97d
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	be8fe1f9-0746-4870-945e-e31bee7b5638
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	ba30bb3f-0ca5-48c1-9300-2ed64e831fb7
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	b789df35-d23f-44fe-95a8-b15cc2ccdb4d
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	9f70277c-eca9-40a9-8696-1e391f3e9084
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	2eae1649-ae40-4a26-a568-af3080aa68f7
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	de838505-439a-4a18-b097-8b4da78c40b6
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	c1ee7caf-a5c9-4dfa-8c19-b021ff7d6a57
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	31637503-7977-46cb-b7c0-886d0e5df9b3
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	4c0daf4c-547c-43b6-b97c-e9e5239c50c6
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	872b6f13-e99f-4192-845b-6bcf00af008d
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	8fc8a4d7-4565-4875-b809-79c8ca5cef2d
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	9850a6e7-1cab-44b5-9380-60af2d378d7c
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	89e45c78-7d5d-42d0-a593-303da90dcd6e
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	8fddc522-5f74-44f1-926d-ab0474158d77
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	ef252cd3-4c1b-4460-8858-2e61ecad9faf
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	1d1c5b03-8539-4c6e-aaef-3d708a84205e
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	7b57fc81-d446-46f4-9e22-9af3e1b8bb9e
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	d2ccd83a-0d16-45fc-a94b-9abe504b4891
2eae1649-ae40-4a26-a568-af3080aa68f7	1d1c5b03-8539-4c6e-aaef-3d708a84205e
9f70277c-eca9-40a9-8696-1e391f3e9084	d2ccd83a-0d16-45fc-a94b-9abe504b4891
9f70277c-eca9-40a9-8696-1e391f3e9084	ef252cd3-4c1b-4460-8858-2e61ecad9faf
6ea2a037-4728-4365-96c9-37d84fded129	740e9a62-6c9d-4d75-b066-032d28b653f8
6ea2a037-4728-4365-96c9-37d84fded129	974a8aad-03c8-42b5-bb5e-3e66e3ab4005
6ea2a037-4728-4365-96c9-37d84fded129	a3883b4d-408f-4c84-bb9c-675eec9fcecc
6ea2a037-4728-4365-96c9-37d84fded129	f7e6f131-76ea-4fd5-b12e-b0fd3372c786
6ea2a037-4728-4365-96c9-37d84fded129	94bdff8e-6517-45b3-96fe-702eb6f5785c
6ea2a037-4728-4365-96c9-37d84fded129	5284ada0-df01-48c6-b62a-1917e29ccf1b
6ea2a037-4728-4365-96c9-37d84fded129	bfb62410-c4f2-4f48-b2fc-3fd77a0bddf8
6ea2a037-4728-4365-96c9-37d84fded129	6c30f82d-ab2e-455b-881a-ec58dd1e93fc
6ea2a037-4728-4365-96c9-37d84fded129	bf00df1f-bcc4-4532-b54b-31fa8f46a0f5
6ea2a037-4728-4365-96c9-37d84fded129	1b6a6021-0bb4-49e1-b560-6557a5779e6a
6ea2a037-4728-4365-96c9-37d84fded129	abb41e03-88ca-4d59-94e3-771abf1521f3
6ea2a037-4728-4365-96c9-37d84fded129	4774953f-3e17-4d7f-bd07-61db843d6e59
6ea2a037-4728-4365-96c9-37d84fded129	fdfa239a-f60f-468a-9a79-a076697cb2a4
6ea2a037-4728-4365-96c9-37d84fded129	ea5ff825-dc29-4d37-ab6f-a914e767f961
6ea2a037-4728-4365-96c9-37d84fded129	74859433-9212-44bf-970b-5f6326110cff
6ea2a037-4728-4365-96c9-37d84fded129	2604207d-4202-4a51-af5c-21f0ee8aa068
6ea2a037-4728-4365-96c9-37d84fded129	368c8826-b2d0-4ffb-83c4-210b1f107eec
f7e6f131-76ea-4fd5-b12e-b0fd3372c786	74859433-9212-44bf-970b-5f6326110cff
a3883b4d-408f-4c84-bb9c-675eec9fcecc	ea5ff825-dc29-4d37-ab6f-a914e767f961
a3883b4d-408f-4c84-bb9c-675eec9fcecc	368c8826-b2d0-4ffb-83c4-210b1f107eec
465f36c0-c7e7-43c9-8ef0-4602ba105a4c	ec49de12-801c-474d-b8da-1bc0f5fada17
465f36c0-c7e7-43c9-8ef0-4602ba105a4c	2f96ec64-a810-4b6b-a2ba-52eea332ebac
2f96ec64-a810-4b6b-a2ba-52eea332ebac	6de9d939-c2ca-49fe-bdb5-24441a3916d4
ac2e2442-3bef-44a2-ac05-3c73f545266a	4eed18bd-996b-4402-8668-67721dbd1c43
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	8403cabe-4bc1-4a33-bfe8-75405d29993e
6ea2a037-4728-4365-96c9-37d84fded129	02b2b995-3132-4ee5-a4c9-accd9db5803b
465f36c0-c7e7-43c9-8ef0-4602ba105a4c	7953c80e-3bc1-4556-a678-704a4d07cb65
465f36c0-c7e7-43c9-8ef0-4602ba105a4c	b99d342a-0f0b-424d-98ca-55e4a3e825ab
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
5a988fe4-3936-4340-a24e-501e493f8665	\N	password	74a38b15-6b6e-49af-ab74-913187ff9c11	1624193316833	\N	{"value":"2sKB/GIQgt5yuEj5MbOdAyEzFa7gxNh85kLAXtpC68RHg5XdJkgGxGbQvEV+yuDGWGRnJVsxqW9bnDKUbIytIQ==","salt":"ay2+1892WH05lsLDXpEWIg==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2021-06-20 12:48:26.934622	1	EXECUTED	7:4e70412f24a3f382c82183742ec79317	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	4193306202
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2021-06-20 12:48:26.953117	2	MARK_RAN	7:cb16724583e9675711801c6875114f28	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	3.5.4	\N	\N	4193306202
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2021-06-20 12:48:27.040932	3	EXECUTED	7:0310eb8ba07cec616460794d42ade0fa	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	3.5.4	\N	\N	4193306202
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2021-06-20 12:48:27.045668	4	EXECUTED	7:5d25857e708c3233ef4439df1f93f012	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	4193306202
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2021-06-20 12:48:27.382288	5	EXECUTED	7:c7a54a1041d58eb3817a4a883b4d4e84	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	4193306202
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2021-06-20 12:48:27.388242	6	MARK_RAN	7:2e01012df20974c1c2a605ef8afe25b7	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	3.5.4	\N	\N	4193306202
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2021-06-20 12:48:27.662143	7	EXECUTED	7:0f08df48468428e0f30ee59a8ec01a41	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	4193306202
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2021-06-20 12:48:27.666659	8	MARK_RAN	7:a77ea2ad226b345e7d689d366f185c8c	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	3.5.4	\N	\N	4193306202
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2021-06-20 12:48:27.67422	9	EXECUTED	7:a3377a2059aefbf3b90ebb4c4cc8e2ab	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	3.5.4	\N	\N	4193306202
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2021-06-20 12:48:27.963586	10	EXECUTED	7:04c1dbedc2aa3e9756d1a1668e003451	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	3.5.4	\N	\N	4193306202
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2021-06-20 12:48:28.088471	11	EXECUTED	7:36ef39ed560ad07062d956db861042ba	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	4193306202
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2021-06-20 12:48:28.091753	12	MARK_RAN	7:d909180b2530479a716d3f9c9eaea3d7	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	4193306202
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2021-06-20 12:48:28.118735	13	EXECUTED	7:cf12b04b79bea5152f165eb41f3955f6	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	3.5.4	\N	\N	4193306202
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-06-20 12:48:28.170364	14	EXECUTED	7:7e32c8f05c755e8675764e7d5f514509	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	3.5.4	\N	\N	4193306202
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-06-20 12:48:28.172194	15	MARK_RAN	7:980ba23cc0ec39cab731ce903dd01291	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	4193306202
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-06-20 12:48:28.173857	16	MARK_RAN	7:2fa220758991285312eb84f3b4ff5336	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	3.5.4	\N	\N	4193306202
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2021-06-20 12:48:28.175228	17	EXECUTED	7:d41d8cd98f00b204e9800998ecf8427e	empty		\N	3.5.4	\N	\N	4193306202
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2021-06-20 12:48:28.297185	18	EXECUTED	7:91ace540896df890cc00a0490ee52bbc	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	3.5.4	\N	\N	4193306202
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2021-06-20 12:48:28.427611	19	EXECUTED	7:c31d1646dfa2618a9335c00e07f89f24	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	4193306202
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2021-06-20 12:48:28.433105	20	EXECUTED	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	4193306202
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-06-20 12:48:30.788881	45	EXECUTED	7:6a48ce645a3525488a90fbf76adf3bb3	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	4193306202
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2021-06-20 12:48:28.435015	21	MARK_RAN	7:f987971fe6b37d963bc95fee2b27f8df	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	3.5.4	\N	\N	4193306202
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2021-06-20 12:48:28.442725	22	MARK_RAN	7:df8bc21027a4f7cbbb01f6344e89ce07	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	3.5.4	\N	\N	4193306202
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2021-06-20 12:48:28.518854	23	EXECUTED	7:ed2dc7f799d19ac452cbcda56c929e47	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	3.5.4	\N	\N	4193306202
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2021-06-20 12:48:28.524099	24	EXECUTED	7:80b5db88a5dda36ece5f235be8757615	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	4193306202
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2021-06-20 12:48:28.525924	25	MARK_RAN	7:1437310ed1305a9b93f8848f301726ce	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	3.5.4	\N	\N	4193306202
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2021-06-20 12:48:29.150076	26	EXECUTED	7:b82ffb34850fa0836be16deefc6a87c4	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	3.5.4	\N	\N	4193306202
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2021-06-20 12:48:29.36513	27	EXECUTED	7:9cc98082921330d8d9266decdd4bd658	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	3.5.4	\N	\N	4193306202
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2021-06-20 12:48:29.370713	28	EXECUTED	7:03d64aeed9cb52b969bd30a7ac0db57e	update tableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	4193306202
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2021-06-20 12:48:29.537342	29	EXECUTED	7:f1f9fd8710399d725b780f463c6b21cd	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	3.5.4	\N	\N	4193306202
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2021-06-20 12:48:29.573381	30	EXECUTED	7:53188c3eb1107546e6f765835705b6c1	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	3.5.4	\N	\N	4193306202
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2021-06-20 12:48:29.607399	31	EXECUTED	7:d6e6f3bc57a0c5586737d1351725d4d4	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	3.5.4	\N	\N	4193306202
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2021-06-20 12:48:29.611592	32	EXECUTED	7:454d604fbd755d9df3fd9c6329043aa5	customChange		\N	3.5.4	\N	\N	4193306202
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-06-20 12:48:29.615982	33	EXECUTED	7:57e98a3077e29caf562f7dbf80c72600	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	4193306202
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-06-20 12:48:29.617463	34	MARK_RAN	7:e4c7e8f2256210aee71ddc42f538b57a	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	4193306202
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-06-20 12:48:29.672103	35	EXECUTED	7:09a43c97e49bc626460480aa1379b522	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	3.5.4	\N	\N	4193306202
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2021-06-20 12:48:29.677098	36	EXECUTED	7:26bfc7c74fefa9126f2ce702fb775553	addColumn tableName=REALM		\N	3.5.4	\N	\N	4193306202
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2021-06-20 12:48:29.685195	37	EXECUTED	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	4193306202
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2021-06-20 12:48:29.689489	38	EXECUTED	7:37fc1781855ac5388c494f1442b3f717	addColumn tableName=FED_USER_CONSENT		\N	3.5.4	\N	\N	4193306202
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2021-06-20 12:48:29.693999	39	EXECUTED	7:13a27db0dae6049541136adad7261d27	addColumn tableName=IDENTITY_PROVIDER		\N	3.5.4	\N	\N	4193306202
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-06-20 12:48:29.695434	40	MARK_RAN	7:550300617e3b59e8af3a6294df8248a3	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	4193306202
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-06-20 12:48:29.697245	41	MARK_RAN	7:e3a9482b8931481dc2772a5c07c44f17	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	3.5.4	\N	\N	4193306202
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2021-06-20 12:48:29.702381	42	EXECUTED	7:72b07d85a2677cb257edb02b408f332d	customChange		\N	3.5.4	\N	\N	4193306202
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2021-06-20 12:48:30.780005	43	EXECUTED	7:a72a7858967bd414835d19e04d880312	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	3.5.4	\N	\N	4193306202
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2021-06-20 12:48:30.784197	44	EXECUTED	7:94edff7cf9ce179e7e85f0cd78a3cf2c	addColumn tableName=USER_ENTITY		\N	3.5.4	\N	\N	4193306202
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-06-20 12:48:30.794373	46	EXECUTED	7:e64b5dcea7db06077c6e57d3b9e5ca14	customChange		\N	3.5.4	\N	\N	4193306202
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-06-20 12:48:30.796077	47	MARK_RAN	7:fd8cf02498f8b1e72496a20afc75178c	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	3.5.4	\N	\N	4193306202
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-06-20 12:48:30.953381	48	EXECUTED	7:542794f25aa2b1fbabb7e577d6646319	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	3.5.4	\N	\N	4193306202
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2021-06-20 12:48:30.957005	49	EXECUTED	7:edad604c882df12f74941dac3cc6d650	addColumn tableName=REALM		\N	3.5.4	\N	\N	4193306202
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2021-06-20 12:48:31.052258	50	EXECUTED	7:0f88b78b7b46480eb92690cbf5e44900	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	3.5.4	\N	\N	4193306202
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2021-06-20 12:48:31.301289	51	EXECUTED	7:d560e43982611d936457c327f872dd59	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	3.5.4	\N	\N	4193306202
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2021-06-20 12:48:31.304195	52	EXECUTED	7:c155566c42b4d14ef07059ec3b3bbd8e	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	4193306202
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2021-06-20 12:48:31.306877	53	EXECUTED	7:b40376581f12d70f3c89ba8ddf5b7dea	update tableName=REALM		\N	3.5.4	\N	\N	4193306202
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2021-06-20 12:48:31.309623	54	EXECUTED	7:a1132cc395f7b95b3646146c2e38f168	update tableName=CLIENT		\N	3.5.4	\N	\N	4193306202
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-06-20 12:48:31.326876	55	EXECUTED	7:d8dc5d89c789105cfa7ca0e82cba60af	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	3.5.4	\N	\N	4193306202
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-06-20 12:48:31.348431	56	EXECUTED	7:7822e0165097182e8f653c35517656a3	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	3.5.4	\N	\N	4193306202
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-06-20 12:48:31.435049	57	EXECUTED	7:c6538c29b9c9a08f9e9ea2de5c2b6375	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	3.5.4	\N	\N	4193306202
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2021-06-20 12:48:31.825959	58	EXECUTED	7:6d4893e36de22369cf73bcb051ded875	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	3.5.4	\N	\N	4193306202
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2021-06-20 12:48:31.898746	59	EXECUTED	7:57960fc0b0f0dd0563ea6f8b2e4a1707	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	3.5.4	\N	\N	4193306202
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2021-06-20 12:48:31.909802	60	EXECUTED	7:2b4b8bff39944c7097977cc18dbceb3b	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	3.5.4	\N	\N	4193306202
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2021-06-20 12:48:31.921603	61	EXECUTED	7:2aa42a964c59cd5b8ca9822340ba33a8	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	3.5.4	\N	\N	4193306202
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2021-06-20 12:48:31.930281	62	EXECUTED	7:9ac9e58545479929ba23f4a3087a0346	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	3.5.4	\N	\N	4193306202
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2021-06-20 12:48:31.932656	63	EXECUTED	7:14d407c35bc4fe1976867756bcea0c36	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	4193306202
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2021-06-20 12:48:31.936103	64	EXECUTED	7:241a8030c748c8548e346adee548fa93	update tableName=REQUIRED_ACTION_PROVIDER		\N	3.5.4	\N	\N	4193306202
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2021-06-20 12:48:31.943472	65	EXECUTED	7:7d3182f65a34fcc61e8d23def037dc3f	update tableName=RESOURCE_SERVER_RESOURCE		\N	3.5.4	\N	\N	4193306202
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2021-06-20 12:48:31.991937	66	EXECUTED	7:b30039e00a0b9715d430d1b0636728fa	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	3.5.4	\N	\N	4193306202
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2021-06-20 12:48:32.023304	67	EXECUTED	7:3797315ca61d531780f8e6f82f258159	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	3.5.4	\N	\N	4193306202
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2021-06-20 12:48:32.028886	68	EXECUTED	7:c7aa4c8d9573500c2d347c1941ff0301	addColumn tableName=REALM		\N	3.5.4	\N	\N	4193306202
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2021-06-20 12:48:32.057577	69	EXECUTED	7:b207faee394fc074a442ecd42185a5dd	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	3.5.4	\N	\N	4193306202
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2021-06-20 12:48:32.062939	70	EXECUTED	7:ab9a9762faaba4ddfa35514b212c4922	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	3.5.4	\N	\N	4193306202
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2021-06-20 12:48:32.072702	71	EXECUTED	7:b9710f74515a6ccb51b72dc0d19df8c4	addColumn tableName=RESOURCE_SERVER		\N	3.5.4	\N	\N	4193306202
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-06-20 12:48:32.080386	72	EXECUTED	7:ec9707ae4d4f0b7452fee20128083879	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	4193306202
8.0.0-updating-credential-data-not-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-06-20 12:48:32.090509	73	EXECUTED	7:3979a0ae07ac465e920ca696532fc736	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	4193306202
8.0.0-updating-credential-data-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-06-20 12:48:32.092426	74	MARK_RAN	7:5abfde4c259119d143bd2fbf49ac2bca	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	3.5.4	\N	\N	4193306202
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-06-20 12:48:32.153582	75	EXECUTED	7:b48da8c11a3d83ddd6b7d0c8c2219345	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	3.5.4	\N	\N	4193306202
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2021-06-20 12:48:32.177176	76	EXECUTED	7:a73379915c23bfad3e8f5c6d5c0aa4bd	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	3.5.4	\N	\N	4193306202
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-06-20 12:48:32.180072	77	EXECUTED	7:39e0073779aba192646291aa2332493d	addColumn tableName=CLIENT		\N	3.5.4	\N	\N	4193306202
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-06-20 12:48:32.181507	78	MARK_RAN	7:81f87368f00450799b4bf42ea0b3ec34	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	3.5.4	\N	\N	4193306202
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-06-20 12:48:32.229097	79	EXECUTED	7:20b37422abb9fb6571c618148f013a15	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	3.5.4	\N	\N	4193306202
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2021-06-20 12:48:32.23073	80	MARK_RAN	7:1970bb6cfb5ee800736b95ad3fb3c78a	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	3.5.4	\N	\N	4193306202
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-06-20 12:48:32.255104	81	EXECUTED	7:45d9b25fc3b455d522d8dcc10a0f4c80	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	3.5.4	\N	\N	4193306202
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-06-20 12:48:32.256513	82	MARK_RAN	7:890ae73712bc187a66c2813a724d037f	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	4193306202
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-06-20 12:48:32.262818	83	EXECUTED	7:0a211980d27fafe3ff50d19a3a29b538	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	4193306202
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-06-20 12:48:32.264253	84	MARK_RAN	7:a161e2ae671a9020fff61e996a207377	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	3.5.4	\N	\N	4193306202
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2021-06-20 12:48:32.29191	85	EXECUTED	7:01c49302201bdf815b0a18d1f98a55dc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	3.5.4	\N	\N	4193306202
map-remove-ri	keycloak	META-INF/jpa-changelog-11.0.0.xml	2021-06-20 12:48:32.298821	86	EXECUTED	7:3dace6b144c11f53f1ad2c0361279b86	dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9		\N	3.5.4	\N	\N	4193306202
map-remove-ri	keycloak	META-INF/jpa-changelog-12.0.0.xml	2021-06-20 12:48:32.311658	87	EXECUTED	7:578d0b92077eaf2ab95ad0ec087aa903	dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...		\N	3.5.4	\N	\N	4193306202
12.1.0-add-realm-localization-table	keycloak	META-INF/jpa-changelog-12.0.0.xml	2021-06-20 12:48:32.335913	88	EXECUTED	7:c95abe90d962c57a09ecaee57972835d	createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS		\N	3.5.4	\N	\N	4193306202
default-roles	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.34304	89	EXECUTED	7:f1313bcc2994a5c4dc1062ed6d8282d3	addColumn tableName=REALM; customChange		\N	3.5.4	\N	\N	4193306202
default-roles-cleanup	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.388996	90	EXECUTED	7:90d763b52eaffebefbcbde55f269508b	dropTable tableName=REALM_DEFAULT_ROLES; dropTable tableName=CLIENT_DEFAULT_ROLES		\N	3.5.4	\N	\N	4193306202
13.0.0-KEYCLOAK-16844	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.414761	91	EXECUTED	7:d554f0cb92b764470dccfa5e0014a7dd	createIndex indexName=IDX_OFFLINE_USS_PRELOAD, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	4193306202
map-remove-ri-13.0.0	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.431538	92	EXECUTED	7:73193e3ab3c35cf0f37ccea3bf783764	dropForeignKeyConstraint baseTableName=DEFAULT_CLIENT_SCOPE, constraintName=FK_R_DEF_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SCOPE_CLIENT, constraintName=FK_C_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SC...		\N	3.5.4	\N	\N	4193306202
13.0.0-KEYCLOAK-17992-drop-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.433171	93	MARK_RAN	7:90a1e74f92e9cbaa0c5eab80b8a037f3	dropPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CLSCOPE_CL, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CL_CLSCOPE, tableName=CLIENT_SCOPE_CLIENT		\N	3.5.4	\N	\N	4193306202
13.0.0-increase-column-size-federated	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.457228	94	EXECUTED	7:5b9248f29cd047c200083cc6d8388b16	modifyDataType columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; modifyDataType columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT		\N	3.5.4	\N	\N	4193306202
13.0.0-KEYCLOAK-17992-recreate-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.459978	95	MARK_RAN	7:64db59e44c374f13955489e8990d17a1	addNotNullConstraint columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; addNotNullConstraint columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT; addPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; createIndex indexName=...		\N	3.5.4	\N	\N	4193306202
json-string-accomodation-fixed	keycloak	META-INF/jpa-changelog-13.0.0.xml	2021-06-20 12:48:32.465918	96	EXECUTED	7:329a578cdb43262fff975f0a7f6cda60	addColumn tableName=REALM_ATTRIBUTE; update tableName=REALM_ATTRIBUTE; dropColumn columnName=VALUE, tableName=REALM_ATTRIBUTE; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=REALM_ATTRIBUTE		\N	3.5.4	\N	\N	4193306202
14.0.0-KEYCLOAK-11019	keycloak	META-INF/jpa-changelog-14.0.0.xml	2021-06-20 12:48:32.530944	97	EXECUTED	7:fae0de241ac0fd0bbc2b380b85e4f567	createIndex indexName=IDX_OFFLINE_CSS_PRELOAD, tableName=OFFLINE_CLIENT_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USER, tableName=OFFLINE_USER_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USERSESS, tableName=OFFLINE_USER_SESSION		\N	3.5.4	\N	\N	4193306202
14.0.0-KEYCLOAK-18286	keycloak	META-INF/jpa-changelog-14.0.0.xml	2021-06-20 12:48:32.553473	98	EXECUTED	7:075d54e9180f49bb0c64ca4218936e81	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	4193306202
14.0.0-KEYCLOAK-18286-mysql	keycloak	META-INF/jpa-changelog-14.0.0.xml	2021-06-20 12:48:32.55528	99	MARK_RAN	7:b558ad47ea0e4d3c3514225a49cc0d65	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	3.5.4	\N	\N	4193306202
KEYCLOAK-17267-add-index-to-user-attributes	keycloak	META-INF/jpa-changelog-14.0.0.xml	2021-06-20 12:48:32.578423	100	EXECUTED	7:1a7f28ff8d9e53aeb879d76ea3d9341a	createIndex indexName=IDX_USER_ATTRIBUTE_NAME, tableName=USER_ATTRIBUTE		\N	3.5.4	\N	\N	4193306202
KEYCLOAK-18146-add-saml-art-binding-identifier	keycloak	META-INF/jpa-changelog-14.0.0.xml	2021-06-20 12:48:32.582958	101	EXECUTED	7:2fd554456fed4a82c698c555c5b751b6	customChange		\N	3.5.4	\N	\N	4193306202
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
master	9ba7abbf-450c-412b-93ec-f73cff93c4da	f
master	f94ef230-1cfe-482f-a183-5ba6e41c7c9a	t
master	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d	t
master	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b	t
master	b364bec7-e5a8-4450-9af5-64c0d643800e	f
master	560030d5-6158-468d-be57-030f61f6b5aa	f
master	4991299f-8091-4483-ab2d-355bb380a4f5	t
master	4e2b10ee-8d1b-4b83-bd89-01d03ca14950	t
master	e960acdb-e83f-4604-8859-9e6fe8dfe0be	f
graphql-demo	e4e2c8aa-74a2-49ba-97b8-c2857982cf01	f
graphql-demo	cdba64eb-1c3f-43c8-a257-240eafa69541	t
graphql-demo	5cf0e0b3-5da6-4111-98cd-e0c683c563c1	t
graphql-demo	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47	t
graphql-demo	d24b6228-f971-4fce-a41c-bc8db682db72	f
graphql-demo	6a756304-fe3d-42bc-b2a8-68e9fa7c273f	f
graphql-demo	f7050e4b-ef73-43ab-9274-3f6056949341	t
graphql-demo	80975af1-7043-4b09-9e0a-cea8849ced2e	t
graphql-demo	19441e8b-6d47-417b-a8d8-794147a5204b	f
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	master	f	${role_default-roles}	default-roles-master	master	\N	\N
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	master	f	${role_admin}	admin	master	\N	\N
b31d5b71-50e2-494f-baf3-c2badf61b943	master	f	${role_create-realm}	create-realm	master	\N	\N
cdc88a97-8096-42d4-aea7-05361497ff1e	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_create-client}	create-client	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
4d18d93b-8591-4eb8-9bf5-fea7b3721955	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-realm}	view-realm	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
4334fa82-2c49-47e2-b651-8c4f69ee9c5d	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-users}	view-users	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
bc90ab75-f5d3-452c-94fa-0221a6250b34	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-clients}	view-clients	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
bcffea9d-d1ab-4494-abed-1f10a54bbd72	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-events}	view-events	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
26984bdd-5ec5-40bc-80d0-6d1596a238e2	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-identity-providers}	view-identity-providers	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
738f5c98-fbac-4f5b-9531-56eb963231ad	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_view-authorization}	view-authorization	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
9805cc37-3954-4fff-8eba-135492d6d220	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-realm}	manage-realm	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
f23c7da3-5ba3-4016-acef-324b2a0875c9	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-users}	manage-users	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
67d30fd4-e228-481d-b054-2bcdb7d0f43c	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-clients}	manage-clients	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
2f3ffbbf-ffb8-49b7-8f83-866df9beaab9	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-events}	manage-events	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
a2594d9c-2050-4b8c-8479-3246b208c897	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-identity-providers}	manage-identity-providers	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
7c951be2-4410-4808-80dd-c1b7471c38a0	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_manage-authorization}	manage-authorization	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
9ee634bc-44cf-4091-886a-7e5044067b92	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_query-users}	query-users	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
279b68de-ed6b-4d42-9232-c0549a4aab0c	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_query-clients}	query-clients	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
6da80ff4-413d-48d3-acee-9268ff8bcef7	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_query-realms}	query-realms	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
13167db1-6986-421d-99ba-eb609672867e	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_query-groups}	query-groups	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
f2e59bb4-0e83-4d94-9b2b-56addac4af12	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_view-profile}	view-profile	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
81219ff6-ecd7-49f4-883d-d272f6bd7e2d	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_manage-account}	manage-account	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
27a33857-e363-41af-86be-06375d5182eb	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_manage-account-links}	manage-account-links	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
6ea2a037-4728-4365-96c9-37d84fded129	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_realm-admin}	realm-admin	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
740e9a62-6c9d-4d75-b066-032d28b653f8	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_create-client}	create-client	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
974a8aad-03c8-42b5-bb5e-3e66e3ab4005	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-realm}	view-realm	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
a3883b4d-408f-4c84-bb9c-675eec9fcecc	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-users}	view-users	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
f7e6f131-76ea-4fd5-b12e-b0fd3372c786	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-clients}	view-clients	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
94bdff8e-6517-45b3-96fe-702eb6f5785c	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-events}	view-events	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
5284ada0-df01-48c6-b62a-1917e29ccf1b	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-identity-providers}	view-identity-providers	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
bfb62410-c4f2-4f48-b2fc-3fd77a0bddf8	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_view-authorization}	view-authorization	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
6c30f82d-ab2e-455b-881a-ec58dd1e93fc	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-realm}	manage-realm	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
17c7f842-4fe8-4a8d-9d72-86ac6cd691c8	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_view-applications}	view-applications	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
8ecde4a4-f358-40ad-ba0c-b7f5f28f7965	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_view-consent}	view-consent	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
167814db-6b10-4daf-8e07-adb8c75e97d5	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_manage-consent}	manage-consent	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
dabb896b-571d-48be-aa2b-d414e6e836b6	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	t	${role_delete-account}	delete-account	master	a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	\N
15ec4fce-9975-4407-9d85-c7b9fa618d83	1821be33-4f8a-43d5-8d4d-d538c440cf17	t	${role_read-token}	read-token	master	1821be33-4f8a-43d5-8d4d-d538c440cf17	\N
81e8710e-9373-4851-8818-05b56a26d97f	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	t	${role_impersonation}	impersonation	master	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	\N
18ae6e54-993e-492d-8ba5-20545debc97d	master	f	${role_offline-access}	offline_access	master	\N	\N
be8fe1f9-0746-4870-945e-e31bee7b5638	master	f	${role_uma_authorization}	uma_authorization	master	\N	\N
465f36c0-c7e7-43c9-8ef0-4602ba105a4c	graphql-demo	f	${role_default-roles}	default-roles-graphql-demo	graphql-demo	\N	\N
ba30bb3f-0ca5-48c1-9300-2ed64e831fb7	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_create-client}	create-client	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
b789df35-d23f-44fe-95a8-b15cc2ccdb4d	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-realm}	view-realm	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
9f70277c-eca9-40a9-8696-1e391f3e9084	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-users}	view-users	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
2eae1649-ae40-4a26-a568-af3080aa68f7	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-clients}	view-clients	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
de838505-439a-4a18-b097-8b4da78c40b6	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-events}	view-events	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
c1ee7caf-a5c9-4dfa-8c19-b021ff7d6a57	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-identity-providers}	view-identity-providers	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
31637503-7977-46cb-b7c0-886d0e5df9b3	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_view-authorization}	view-authorization	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
4c0daf4c-547c-43b6-b97c-e9e5239c50c6	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-realm}	manage-realm	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
872b6f13-e99f-4192-845b-6bcf00af008d	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-users}	manage-users	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
8fc8a4d7-4565-4875-b809-79c8ca5cef2d	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-clients}	manage-clients	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
9850a6e7-1cab-44b5-9380-60af2d378d7c	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-events}	manage-events	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
89e45c78-7d5d-42d0-a593-303da90dcd6e	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-identity-providers}	manage-identity-providers	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
8fddc522-5f74-44f1-926d-ab0474158d77	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_manage-authorization}	manage-authorization	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
ef252cd3-4c1b-4460-8858-2e61ecad9faf	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_query-users}	query-users	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
1d1c5b03-8539-4c6e-aaef-3d708a84205e	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_query-clients}	query-clients	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
7b57fc81-d446-46f4-9e22-9af3e1b8bb9e	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_query-realms}	query-realms	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
d2ccd83a-0d16-45fc-a94b-9abe504b4891	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_query-groups}	query-groups	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
bf00df1f-bcc4-4532-b54b-31fa8f46a0f5	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-users}	manage-users	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
1b6a6021-0bb4-49e1-b560-6557a5779e6a	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-clients}	manage-clients	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
abb41e03-88ca-4d59-94e3-771abf1521f3	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-events}	manage-events	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
4774953f-3e17-4d7f-bd07-61db843d6e59	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-identity-providers}	manage-identity-providers	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
fdfa239a-f60f-468a-9a79-a076697cb2a4	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_manage-authorization}	manage-authorization	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
ea5ff825-dc29-4d37-ab6f-a914e767f961	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_query-users}	query-users	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
74859433-9212-44bf-970b-5f6326110cff	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_query-clients}	query-clients	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
2604207d-4202-4a51-af5c-21f0ee8aa068	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_query-realms}	query-realms	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
368c8826-b2d0-4ffb-83c4-210b1f107eec	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_query-groups}	query-groups	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
ec49de12-801c-474d-b8da-1bc0f5fada17	316254a7-7597-4eab-bfca-6e492adce061	t	${role_view-profile}	view-profile	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
2f96ec64-a810-4b6b-a2ba-52eea332ebac	316254a7-7597-4eab-bfca-6e492adce061	t	${role_manage-account}	manage-account	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
6de9d939-c2ca-49fe-bdb5-24441a3916d4	316254a7-7597-4eab-bfca-6e492adce061	t	${role_manage-account-links}	manage-account-links	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
276653ff-1e47-48ba-b5cb-6b9c57cfd30c	316254a7-7597-4eab-bfca-6e492adce061	t	${role_view-applications}	view-applications	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
4eed18bd-996b-4402-8668-67721dbd1c43	316254a7-7597-4eab-bfca-6e492adce061	t	${role_view-consent}	view-consent	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
ac2e2442-3bef-44a2-ac05-3c73f545266a	316254a7-7597-4eab-bfca-6e492adce061	t	${role_manage-consent}	manage-consent	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
2c680b83-217d-4b19-a260-8c780a648543	316254a7-7597-4eab-bfca-6e492adce061	t	${role_delete-account}	delete-account	graphql-demo	316254a7-7597-4eab-bfca-6e492adce061	\N
8403cabe-4bc1-4a33-bfe8-75405d29993e	6d4ea0ce-66db-4183-939d-426781120ab2	t	${role_impersonation}	impersonation	master	6d4ea0ce-66db-4183-939d-426781120ab2	\N
02b2b995-3132-4ee5-a4c9-accd9db5803b	f7e7b3b6-64d8-4236-9599-102c5a33323e	t	${role_impersonation}	impersonation	graphql-demo	f7e7b3b6-64d8-4236-9599-102c5a33323e	\N
4fd7e5fc-7f9a-4592-b7c2-6cfa6f94ffac	583c55f0-6d14-45a7-98b0-b36bac7cf5e4	t	${role_read-token}	read-token	graphql-demo	583c55f0-6d14-45a7-98b0-b36bac7cf5e4	\N
7953c80e-3bc1-4556-a678-704a4d07cb65	graphql-demo	f	${role_offline-access}	offline_access	graphql-demo	\N	\N
b99d342a-0f0b-424d-98ca-55e4a3e825ab	graphql-demo	f	${role_uma_authorization}	uma_authorization	graphql-demo	\N	\N
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.migration_model (id, version, update_time) FROM stdin;
c9qxm	14.0.0	1624193314
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
af129649-609f-4ee4-99bd-71bb6368bb44	audience resolve	openid-connect	oidc-audience-resolve-mapper	95089ef3-abe8-41ed-8139-2d06fe16544f	\N
39949cd1-2a75-4e85-8613-e89df46459ee	locale	openid-connect	oidc-usermodel-attribute-mapper	4866beda-e11d-43f5-b327-7be9abd38666	\N
ee5ae452-efbf-4cf9-a562-fe9ec004e298	role list	saml	saml-role-list-mapper	\N	f94ef230-1cfe-482f-a183-5ba6e41c7c9a
3e93e8a1-671e-49ea-9bfe-1319f4cfa919	full name	openid-connect	oidc-full-name-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	family name	openid-connect	oidc-usermodel-property-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
3c8690f8-9116-4cdb-ae4c-199b520e7596	given name	openid-connect	oidc-usermodel-property-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
59513de8-9d84-4fd5-aab8-7731ccfb89c3	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
b97abec5-f493-464f-a692-b615486d67f0	username	openid-connect	oidc-usermodel-property-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
9d6b5994-4d42-49fb-a078-1e50f3322331	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
ec9dbee4-13f8-4b77-998a-61d457884c21	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	website	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
de3db754-8b72-4404-93e0-526df88c9eb4	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
32076917-742e-4f5a-9963-da07482ec13c	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
35341eae-06ae-4475-bbce-477df0f38ab0	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
8f1af2df-1145-49fc-80e6-8881199b2d21	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
0d63a408-f0a4-4240-93fe-6f475b619770	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	d2edbf4b-1432-44cb-ac27-fc8fb5ea1b6d
8f56aad8-e744-41ca-89fb-ff9043f72b66	email	openid-connect	oidc-usermodel-property-mapper	\N	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	email verified	openid-connect	oidc-usermodel-property-mapper	\N	f2cf74d2-e94c-4e72-a1b1-01a6e802a62b
b0c34dc6-a262-4743-aa4a-58ce2d369811	address	openid-connect	oidc-address-mapper	\N	b364bec7-e5a8-4450-9af5-64c0d643800e
04c6bde2-a994-46b0-9d5b-26244dca7845	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	560030d5-6158-468d-be57-030f61f6b5aa
e0fb0077-a18b-4c84-b259-358ff91790f3	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	560030d5-6158-468d-be57-030f61f6b5aa
67098184-64d6-4144-b68a-d0ddbb9de3ac	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	4991299f-8091-4483-ab2d-355bb380a4f5
9bc45cb3-c92c-4fff-b903-4cefb5271915	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	4991299f-8091-4483-ab2d-355bb380a4f5
76cf2b6c-5cf2-47b3-ab9c-95b9c2d14320	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	4991299f-8091-4483-ab2d-355bb380a4f5
1becba6c-95cc-4327-a3d3-7a92bd9ec6d2	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	4e2b10ee-8d1b-4b83-bd89-01d03ca14950
a0436cfa-185e-4514-9838-ee67941e37d2	upn	openid-connect	oidc-usermodel-property-mapper	\N	e960acdb-e83f-4604-8859-9e6fe8dfe0be
23dcfa84-b149-4c0e-9c59-69a33a728a2b	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	e960acdb-e83f-4604-8859-9e6fe8dfe0be
027aa48e-ab39-4346-90de-f415fc56cfcc	audience resolve	openid-connect	oidc-audience-resolve-mapper	b369c12d-3d77-423a-891d-70a3164948cc	\N
214452b5-377f-42b9-bb06-9033f944bac1	role list	saml	saml-role-list-mapper	\N	cdba64eb-1c3f-43c8-a257-240eafa69541
6d67f57e-3a26-4b76-8cdf-899619657144	full name	openid-connect	oidc-full-name-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
d9d26219-4ad9-4213-8c0a-ae32956455e7	family name	openid-connect	oidc-usermodel-property-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
fccc7b46-3508-4bd0-b945-6f7a3393c297	given name	openid-connect	oidc-usermodel-property-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
a8970763-fa16-420f-be34-61a07e1dece7	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
0843d112-9f65-47ce-b77e-5c6200896e72	username	openid-connect	oidc-usermodel-property-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
f39b6112-3385-496c-9f57-cabdaaabbff4	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
53bee962-45f8-4564-b5d3-04d412fc88f1	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
3567aeff-fa70-47f7-bb7c-742af7f60bc4	website	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
71a4d531-074c-4c5b-b474-649df825092c	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
ac23d140-aa25-47a7-a297-e3a6ff74f686	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
a064c560-a219-47d2-815f-25df9155d736	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	5cf0e0b3-5da6-4111-98cd-e0c683c563c1
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	email	openid-connect	oidc-usermodel-property-mapper	\N	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47
60a06778-84be-4ed7-aa54-e94bd39ea979	email verified	openid-connect	oidc-usermodel-property-mapper	\N	6d07b43e-5ac5-4dce-b8e2-d072c0bf5d47
64634d92-329e-4b2b-b262-e5da59448db0	address	openid-connect	oidc-address-mapper	\N	d24b6228-f971-4fce-a41c-bc8db682db72
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	6a756304-fe3d-42bc-b2a8-68e9fa7c273f
60e163b7-6aeb-42e9-a157-d7b3b05b6442	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	6a756304-fe3d-42bc-b2a8-68e9fa7c273f
0ee4b8e1-ace7-4270-b637-b5a7db55965e	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	f7050e4b-ef73-43ab-9274-3f6056949341
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	f7050e4b-ef73-43ab-9274-3f6056949341
2c2585fa-40dd-449f-a7ed-28c6dd4e9e9f	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	f7050e4b-ef73-43ab-9274-3f6056949341
0e4b304a-3d2e-4169-8bc1-ff9406d0e311	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	80975af1-7043-4b09-9e0a-cea8849ced2e
8ae12fd4-ff58-4f26-975e-5396ffe793a3	upn	openid-connect	oidc-usermodel-property-mapper	\N	19441e8b-6d47-417b-a8d8-794147a5204b
9bd03fa2-c37c-4cb2-8015-680df74f8c50	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	19441e8b-6d47-417b-a8d8-794147a5204b
45b66e57-7829-4d1e-8b1e-50275900e0d0	locale	openid-connect	oidc-usermodel-attribute-mapper	30a27344-f11c-4837-926d-324134b7b6fe	\N
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
39949cd1-2a75-4e85-8613-e89df46459ee	true	userinfo.token.claim
39949cd1-2a75-4e85-8613-e89df46459ee	locale	user.attribute
39949cd1-2a75-4e85-8613-e89df46459ee	true	id.token.claim
39949cd1-2a75-4e85-8613-e89df46459ee	true	access.token.claim
39949cd1-2a75-4e85-8613-e89df46459ee	locale	claim.name
39949cd1-2a75-4e85-8613-e89df46459ee	String	jsonType.label
ee5ae452-efbf-4cf9-a562-fe9ec004e298	false	single
ee5ae452-efbf-4cf9-a562-fe9ec004e298	Basic	attribute.nameformat
ee5ae452-efbf-4cf9-a562-fe9ec004e298	Role	attribute.name
3e93e8a1-671e-49ea-9bfe-1319f4cfa919	true	userinfo.token.claim
3e93e8a1-671e-49ea-9bfe-1319f4cfa919	true	id.token.claim
3e93e8a1-671e-49ea-9bfe-1319f4cfa919	true	access.token.claim
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	true	userinfo.token.claim
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	lastName	user.attribute
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	true	id.token.claim
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	true	access.token.claim
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	family_name	claim.name
e8af798f-0ffb-4f37-ae2d-7b6ee8bd6866	String	jsonType.label
3c8690f8-9116-4cdb-ae4c-199b520e7596	true	userinfo.token.claim
3c8690f8-9116-4cdb-ae4c-199b520e7596	firstName	user.attribute
3c8690f8-9116-4cdb-ae4c-199b520e7596	true	id.token.claim
3c8690f8-9116-4cdb-ae4c-199b520e7596	true	access.token.claim
3c8690f8-9116-4cdb-ae4c-199b520e7596	given_name	claim.name
3c8690f8-9116-4cdb-ae4c-199b520e7596	String	jsonType.label
59513de8-9d84-4fd5-aab8-7731ccfb89c3	true	userinfo.token.claim
59513de8-9d84-4fd5-aab8-7731ccfb89c3	middleName	user.attribute
59513de8-9d84-4fd5-aab8-7731ccfb89c3	true	id.token.claim
59513de8-9d84-4fd5-aab8-7731ccfb89c3	true	access.token.claim
59513de8-9d84-4fd5-aab8-7731ccfb89c3	middle_name	claim.name
59513de8-9d84-4fd5-aab8-7731ccfb89c3	String	jsonType.label
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	true	userinfo.token.claim
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	nickname	user.attribute
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	true	id.token.claim
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	true	access.token.claim
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	nickname	claim.name
991ec450-3e1a-422e-9a2d-fe28b4ef74a0	String	jsonType.label
b97abec5-f493-464f-a692-b615486d67f0	true	userinfo.token.claim
b97abec5-f493-464f-a692-b615486d67f0	username	user.attribute
b97abec5-f493-464f-a692-b615486d67f0	true	id.token.claim
b97abec5-f493-464f-a692-b615486d67f0	true	access.token.claim
b97abec5-f493-464f-a692-b615486d67f0	preferred_username	claim.name
b97abec5-f493-464f-a692-b615486d67f0	String	jsonType.label
9d6b5994-4d42-49fb-a078-1e50f3322331	true	userinfo.token.claim
9d6b5994-4d42-49fb-a078-1e50f3322331	profile	user.attribute
9d6b5994-4d42-49fb-a078-1e50f3322331	true	id.token.claim
9d6b5994-4d42-49fb-a078-1e50f3322331	true	access.token.claim
9d6b5994-4d42-49fb-a078-1e50f3322331	profile	claim.name
9d6b5994-4d42-49fb-a078-1e50f3322331	String	jsonType.label
ec9dbee4-13f8-4b77-998a-61d457884c21	true	userinfo.token.claim
ec9dbee4-13f8-4b77-998a-61d457884c21	picture	user.attribute
ec9dbee4-13f8-4b77-998a-61d457884c21	true	id.token.claim
ec9dbee4-13f8-4b77-998a-61d457884c21	true	access.token.claim
ec9dbee4-13f8-4b77-998a-61d457884c21	picture	claim.name
ec9dbee4-13f8-4b77-998a-61d457884c21	String	jsonType.label
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	true	userinfo.token.claim
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	website	user.attribute
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	true	id.token.claim
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	true	access.token.claim
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	website	claim.name
6c9aa5fc-3556-4c93-b4d6-ebbbc8467c14	String	jsonType.label
de3db754-8b72-4404-93e0-526df88c9eb4	true	userinfo.token.claim
de3db754-8b72-4404-93e0-526df88c9eb4	gender	user.attribute
de3db754-8b72-4404-93e0-526df88c9eb4	true	id.token.claim
de3db754-8b72-4404-93e0-526df88c9eb4	true	access.token.claim
de3db754-8b72-4404-93e0-526df88c9eb4	gender	claim.name
de3db754-8b72-4404-93e0-526df88c9eb4	String	jsonType.label
32076917-742e-4f5a-9963-da07482ec13c	true	userinfo.token.claim
32076917-742e-4f5a-9963-da07482ec13c	birthdate	user.attribute
32076917-742e-4f5a-9963-da07482ec13c	true	id.token.claim
32076917-742e-4f5a-9963-da07482ec13c	true	access.token.claim
32076917-742e-4f5a-9963-da07482ec13c	birthdate	claim.name
32076917-742e-4f5a-9963-da07482ec13c	String	jsonType.label
35341eae-06ae-4475-bbce-477df0f38ab0	true	userinfo.token.claim
35341eae-06ae-4475-bbce-477df0f38ab0	zoneinfo	user.attribute
35341eae-06ae-4475-bbce-477df0f38ab0	true	id.token.claim
35341eae-06ae-4475-bbce-477df0f38ab0	true	access.token.claim
35341eae-06ae-4475-bbce-477df0f38ab0	zoneinfo	claim.name
35341eae-06ae-4475-bbce-477df0f38ab0	String	jsonType.label
8f1af2df-1145-49fc-80e6-8881199b2d21	true	userinfo.token.claim
8f1af2df-1145-49fc-80e6-8881199b2d21	locale	user.attribute
8f1af2df-1145-49fc-80e6-8881199b2d21	true	id.token.claim
8f1af2df-1145-49fc-80e6-8881199b2d21	true	access.token.claim
8f1af2df-1145-49fc-80e6-8881199b2d21	locale	claim.name
8f1af2df-1145-49fc-80e6-8881199b2d21	String	jsonType.label
0d63a408-f0a4-4240-93fe-6f475b619770	true	userinfo.token.claim
0d63a408-f0a4-4240-93fe-6f475b619770	updatedAt	user.attribute
0d63a408-f0a4-4240-93fe-6f475b619770	true	id.token.claim
0d63a408-f0a4-4240-93fe-6f475b619770	true	access.token.claim
0d63a408-f0a4-4240-93fe-6f475b619770	updated_at	claim.name
0d63a408-f0a4-4240-93fe-6f475b619770	String	jsonType.label
8f56aad8-e744-41ca-89fb-ff9043f72b66	true	userinfo.token.claim
8f56aad8-e744-41ca-89fb-ff9043f72b66	email	user.attribute
8f56aad8-e744-41ca-89fb-ff9043f72b66	true	id.token.claim
8f56aad8-e744-41ca-89fb-ff9043f72b66	true	access.token.claim
8f56aad8-e744-41ca-89fb-ff9043f72b66	email	claim.name
8f56aad8-e744-41ca-89fb-ff9043f72b66	String	jsonType.label
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	true	userinfo.token.claim
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	emailVerified	user.attribute
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	true	id.token.claim
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	true	access.token.claim
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	email_verified	claim.name
a06ecd63-8e6c-4d05-a475-e5a28d277e3b	boolean	jsonType.label
b0c34dc6-a262-4743-aa4a-58ce2d369811	formatted	user.attribute.formatted
b0c34dc6-a262-4743-aa4a-58ce2d369811	country	user.attribute.country
b0c34dc6-a262-4743-aa4a-58ce2d369811	postal_code	user.attribute.postal_code
b0c34dc6-a262-4743-aa4a-58ce2d369811	true	userinfo.token.claim
b0c34dc6-a262-4743-aa4a-58ce2d369811	street	user.attribute.street
b0c34dc6-a262-4743-aa4a-58ce2d369811	true	id.token.claim
b0c34dc6-a262-4743-aa4a-58ce2d369811	region	user.attribute.region
b0c34dc6-a262-4743-aa4a-58ce2d369811	true	access.token.claim
b0c34dc6-a262-4743-aa4a-58ce2d369811	locality	user.attribute.locality
04c6bde2-a994-46b0-9d5b-26244dca7845	true	userinfo.token.claim
04c6bde2-a994-46b0-9d5b-26244dca7845	phoneNumber	user.attribute
04c6bde2-a994-46b0-9d5b-26244dca7845	true	id.token.claim
04c6bde2-a994-46b0-9d5b-26244dca7845	true	access.token.claim
04c6bde2-a994-46b0-9d5b-26244dca7845	phone_number	claim.name
04c6bde2-a994-46b0-9d5b-26244dca7845	String	jsonType.label
e0fb0077-a18b-4c84-b259-358ff91790f3	true	userinfo.token.claim
e0fb0077-a18b-4c84-b259-358ff91790f3	phoneNumberVerified	user.attribute
e0fb0077-a18b-4c84-b259-358ff91790f3	true	id.token.claim
e0fb0077-a18b-4c84-b259-358ff91790f3	true	access.token.claim
e0fb0077-a18b-4c84-b259-358ff91790f3	phone_number_verified	claim.name
e0fb0077-a18b-4c84-b259-358ff91790f3	boolean	jsonType.label
67098184-64d6-4144-b68a-d0ddbb9de3ac	true	multivalued
67098184-64d6-4144-b68a-d0ddbb9de3ac	foo	user.attribute
67098184-64d6-4144-b68a-d0ddbb9de3ac	true	access.token.claim
67098184-64d6-4144-b68a-d0ddbb9de3ac	realm_access.roles	claim.name
67098184-64d6-4144-b68a-d0ddbb9de3ac	String	jsonType.label
9bc45cb3-c92c-4fff-b903-4cefb5271915	true	multivalued
9bc45cb3-c92c-4fff-b903-4cefb5271915	foo	user.attribute
9bc45cb3-c92c-4fff-b903-4cefb5271915	true	access.token.claim
9bc45cb3-c92c-4fff-b903-4cefb5271915	resource_access.${client_id}.roles	claim.name
9bc45cb3-c92c-4fff-b903-4cefb5271915	String	jsonType.label
a0436cfa-185e-4514-9838-ee67941e37d2	true	userinfo.token.claim
a0436cfa-185e-4514-9838-ee67941e37d2	username	user.attribute
a0436cfa-185e-4514-9838-ee67941e37d2	true	id.token.claim
a0436cfa-185e-4514-9838-ee67941e37d2	true	access.token.claim
a0436cfa-185e-4514-9838-ee67941e37d2	upn	claim.name
a0436cfa-185e-4514-9838-ee67941e37d2	String	jsonType.label
23dcfa84-b149-4c0e-9c59-69a33a728a2b	true	multivalued
23dcfa84-b149-4c0e-9c59-69a33a728a2b	foo	user.attribute
23dcfa84-b149-4c0e-9c59-69a33a728a2b	true	id.token.claim
23dcfa84-b149-4c0e-9c59-69a33a728a2b	true	access.token.claim
23dcfa84-b149-4c0e-9c59-69a33a728a2b	groups	claim.name
23dcfa84-b149-4c0e-9c59-69a33a728a2b	String	jsonType.label
214452b5-377f-42b9-bb06-9033f944bac1	false	single
214452b5-377f-42b9-bb06-9033f944bac1	Basic	attribute.nameformat
214452b5-377f-42b9-bb06-9033f944bac1	Role	attribute.name
6d67f57e-3a26-4b76-8cdf-899619657144	true	userinfo.token.claim
6d67f57e-3a26-4b76-8cdf-899619657144	true	id.token.claim
6d67f57e-3a26-4b76-8cdf-899619657144	true	access.token.claim
d9d26219-4ad9-4213-8c0a-ae32956455e7	true	userinfo.token.claim
d9d26219-4ad9-4213-8c0a-ae32956455e7	lastName	user.attribute
d9d26219-4ad9-4213-8c0a-ae32956455e7	true	id.token.claim
d9d26219-4ad9-4213-8c0a-ae32956455e7	true	access.token.claim
d9d26219-4ad9-4213-8c0a-ae32956455e7	family_name	claim.name
d9d26219-4ad9-4213-8c0a-ae32956455e7	String	jsonType.label
fccc7b46-3508-4bd0-b945-6f7a3393c297	true	userinfo.token.claim
fccc7b46-3508-4bd0-b945-6f7a3393c297	firstName	user.attribute
fccc7b46-3508-4bd0-b945-6f7a3393c297	true	id.token.claim
fccc7b46-3508-4bd0-b945-6f7a3393c297	true	access.token.claim
fccc7b46-3508-4bd0-b945-6f7a3393c297	given_name	claim.name
fccc7b46-3508-4bd0-b945-6f7a3393c297	String	jsonType.label
a8970763-fa16-420f-be34-61a07e1dece7	true	userinfo.token.claim
a8970763-fa16-420f-be34-61a07e1dece7	middleName	user.attribute
a8970763-fa16-420f-be34-61a07e1dece7	true	id.token.claim
a8970763-fa16-420f-be34-61a07e1dece7	true	access.token.claim
a8970763-fa16-420f-be34-61a07e1dece7	middle_name	claim.name
a8970763-fa16-420f-be34-61a07e1dece7	String	jsonType.label
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	true	userinfo.token.claim
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	nickname	user.attribute
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	true	id.token.claim
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	true	access.token.claim
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	nickname	claim.name
eafbe3a5-c898-4ab3-b1d7-bd4068687a93	String	jsonType.label
0843d112-9f65-47ce-b77e-5c6200896e72	true	userinfo.token.claim
0843d112-9f65-47ce-b77e-5c6200896e72	username	user.attribute
0843d112-9f65-47ce-b77e-5c6200896e72	true	id.token.claim
0843d112-9f65-47ce-b77e-5c6200896e72	true	access.token.claim
0843d112-9f65-47ce-b77e-5c6200896e72	preferred_username	claim.name
0843d112-9f65-47ce-b77e-5c6200896e72	String	jsonType.label
f39b6112-3385-496c-9f57-cabdaaabbff4	true	userinfo.token.claim
f39b6112-3385-496c-9f57-cabdaaabbff4	profile	user.attribute
f39b6112-3385-496c-9f57-cabdaaabbff4	true	id.token.claim
f39b6112-3385-496c-9f57-cabdaaabbff4	true	access.token.claim
f39b6112-3385-496c-9f57-cabdaaabbff4	profile	claim.name
f39b6112-3385-496c-9f57-cabdaaabbff4	String	jsonType.label
53bee962-45f8-4564-b5d3-04d412fc88f1	true	userinfo.token.claim
53bee962-45f8-4564-b5d3-04d412fc88f1	picture	user.attribute
53bee962-45f8-4564-b5d3-04d412fc88f1	true	id.token.claim
53bee962-45f8-4564-b5d3-04d412fc88f1	true	access.token.claim
53bee962-45f8-4564-b5d3-04d412fc88f1	picture	claim.name
53bee962-45f8-4564-b5d3-04d412fc88f1	String	jsonType.label
3567aeff-fa70-47f7-bb7c-742af7f60bc4	true	userinfo.token.claim
3567aeff-fa70-47f7-bb7c-742af7f60bc4	website	user.attribute
3567aeff-fa70-47f7-bb7c-742af7f60bc4	true	id.token.claim
3567aeff-fa70-47f7-bb7c-742af7f60bc4	true	access.token.claim
3567aeff-fa70-47f7-bb7c-742af7f60bc4	website	claim.name
3567aeff-fa70-47f7-bb7c-742af7f60bc4	String	jsonType.label
71a4d531-074c-4c5b-b474-649df825092c	true	userinfo.token.claim
71a4d531-074c-4c5b-b474-649df825092c	gender	user.attribute
71a4d531-074c-4c5b-b474-649df825092c	true	id.token.claim
71a4d531-074c-4c5b-b474-649df825092c	true	access.token.claim
71a4d531-074c-4c5b-b474-649df825092c	gender	claim.name
71a4d531-074c-4c5b-b474-649df825092c	String	jsonType.label
ac23d140-aa25-47a7-a297-e3a6ff74f686	true	userinfo.token.claim
ac23d140-aa25-47a7-a297-e3a6ff74f686	birthdate	user.attribute
ac23d140-aa25-47a7-a297-e3a6ff74f686	true	id.token.claim
ac23d140-aa25-47a7-a297-e3a6ff74f686	true	access.token.claim
ac23d140-aa25-47a7-a297-e3a6ff74f686	birthdate	claim.name
ac23d140-aa25-47a7-a297-e3a6ff74f686	String	jsonType.label
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	true	userinfo.token.claim
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	zoneinfo	user.attribute
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	true	id.token.claim
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	true	access.token.claim
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	zoneinfo	claim.name
2c9c67ba-08d9-4943-8d1a-9d1f19a002a7	String	jsonType.label
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	true	userinfo.token.claim
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	locale	user.attribute
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	true	id.token.claim
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	true	access.token.claim
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	locale	claim.name
692fcf1b-d6a1-4ecc-aec0-248e3b1dd54e	String	jsonType.label
a064c560-a219-47d2-815f-25df9155d736	true	userinfo.token.claim
a064c560-a219-47d2-815f-25df9155d736	updatedAt	user.attribute
a064c560-a219-47d2-815f-25df9155d736	true	id.token.claim
a064c560-a219-47d2-815f-25df9155d736	true	access.token.claim
a064c560-a219-47d2-815f-25df9155d736	updated_at	claim.name
a064c560-a219-47d2-815f-25df9155d736	String	jsonType.label
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	true	userinfo.token.claim
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	email	user.attribute
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	true	id.token.claim
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	true	access.token.claim
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	email	claim.name
6e201f6d-fb2a-4905-bbb1-b6c78c2012b9	String	jsonType.label
60a06778-84be-4ed7-aa54-e94bd39ea979	true	userinfo.token.claim
60a06778-84be-4ed7-aa54-e94bd39ea979	emailVerified	user.attribute
60a06778-84be-4ed7-aa54-e94bd39ea979	true	id.token.claim
60a06778-84be-4ed7-aa54-e94bd39ea979	true	access.token.claim
60a06778-84be-4ed7-aa54-e94bd39ea979	email_verified	claim.name
60a06778-84be-4ed7-aa54-e94bd39ea979	boolean	jsonType.label
64634d92-329e-4b2b-b262-e5da59448db0	formatted	user.attribute.formatted
64634d92-329e-4b2b-b262-e5da59448db0	country	user.attribute.country
64634d92-329e-4b2b-b262-e5da59448db0	postal_code	user.attribute.postal_code
64634d92-329e-4b2b-b262-e5da59448db0	true	userinfo.token.claim
64634d92-329e-4b2b-b262-e5da59448db0	street	user.attribute.street
64634d92-329e-4b2b-b262-e5da59448db0	true	id.token.claim
64634d92-329e-4b2b-b262-e5da59448db0	region	user.attribute.region
64634d92-329e-4b2b-b262-e5da59448db0	true	access.token.claim
64634d92-329e-4b2b-b262-e5da59448db0	locality	user.attribute.locality
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	true	userinfo.token.claim
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	phoneNumber	user.attribute
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	true	id.token.claim
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	true	access.token.claim
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	phone_number	claim.name
8096f4d5-01e5-4e70-bed5-cd0cc8a68da9	String	jsonType.label
60e163b7-6aeb-42e9-a157-d7b3b05b6442	true	userinfo.token.claim
60e163b7-6aeb-42e9-a157-d7b3b05b6442	phoneNumberVerified	user.attribute
60e163b7-6aeb-42e9-a157-d7b3b05b6442	true	id.token.claim
60e163b7-6aeb-42e9-a157-d7b3b05b6442	true	access.token.claim
60e163b7-6aeb-42e9-a157-d7b3b05b6442	phone_number_verified	claim.name
60e163b7-6aeb-42e9-a157-d7b3b05b6442	boolean	jsonType.label
0ee4b8e1-ace7-4270-b637-b5a7db55965e	true	multivalued
0ee4b8e1-ace7-4270-b637-b5a7db55965e	foo	user.attribute
0ee4b8e1-ace7-4270-b637-b5a7db55965e	true	access.token.claim
0ee4b8e1-ace7-4270-b637-b5a7db55965e	realm_access.roles	claim.name
0ee4b8e1-ace7-4270-b637-b5a7db55965e	String	jsonType.label
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	true	multivalued
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	foo	user.attribute
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	true	access.token.claim
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	resource_access.${client_id}.roles	claim.name
b89eaf46-1efa-4ef1-b6e0-765ab79a7738	String	jsonType.label
8ae12fd4-ff58-4f26-975e-5396ffe793a3	true	userinfo.token.claim
8ae12fd4-ff58-4f26-975e-5396ffe793a3	username	user.attribute
8ae12fd4-ff58-4f26-975e-5396ffe793a3	true	id.token.claim
8ae12fd4-ff58-4f26-975e-5396ffe793a3	true	access.token.claim
8ae12fd4-ff58-4f26-975e-5396ffe793a3	upn	claim.name
8ae12fd4-ff58-4f26-975e-5396ffe793a3	String	jsonType.label
9bd03fa2-c37c-4cb2-8015-680df74f8c50	true	multivalued
9bd03fa2-c37c-4cb2-8015-680df74f8c50	foo	user.attribute
9bd03fa2-c37c-4cb2-8015-680df74f8c50	true	id.token.claim
9bd03fa2-c37c-4cb2-8015-680df74f8c50	true	access.token.claim
9bd03fa2-c37c-4cb2-8015-680df74f8c50	groups	claim.name
9bd03fa2-c37c-4cb2-8015-680df74f8c50	String	jsonType.label
45b66e57-7829-4d1e-8b1e-50275900e0d0	true	userinfo.token.claim
45b66e57-7829-4d1e-8b1e-50275900e0d0	locale	user.attribute
45b66e57-7829-4d1e-8b1e-50275900e0d0	true	id.token.claim
45b66e57-7829-4d1e-8b1e-50275900e0d0	true	access.token.claim
45b66e57-7829-4d1e-8b1e-50275900e0d0	locale	claim.name
45b66e57-7829-4d1e-8b1e-50275900e0d0	String	jsonType.label
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me, default_role) FROM stdin;
master	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	3eb45ce4-6534-4a87-9a6f-5a4ef71c300d	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	2d2f2420-e60b-4145-8e40-91d44beb6469	d5712b39-504b-43e9-84a8-aa03e5b95452	a7bddd0c-fb4c-4bba-bb0d-95bc2e4cf1e1	023b6442-7390-444a-b57e-f6f4618e2cb4	bf511b80-3e1b-4779-8f86-0186a12c5a05	2592000	f	900	t	f	14a5bf84-b55e-4298-a77b-38bafb56a1a5	0	f	0	0	14a34700-0eaa-4ccc-bd85-df5055bfa4d4
graphql-demo	60	300	300	\N	\N	\N	t	f	0	\N	graphql-demo	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	6d4ea0ce-66db-4183-939d-426781120ab2	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	54c69e5b-221d-4a8c-a055-0e9b317aa57b	00460774-743c-4bf9-b0fd-dcf205cbd2fc	17e47e05-936d-4e9b-a94e-fd7d1dd44e3c	35df30ca-39e9-4add-a493-716b7491753e	34472a09-8ea4-42c6-a179-ee09e80684ba	2592000	f	900	t	f	d9e99bec-4dbf-4fc8-aec5-29c0064fddac	0	f	0	0	465f36c0-c7e7-43c9-8ef0-4602ba105a4c
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_attribute (name, realm_id, value) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly	master	
_browser_header.xContentTypeOptions	master	nosniff
_browser_header.xRobotsTag	master	none
_browser_header.xFrameOptions	master	SAMEORIGIN
_browser_header.contentSecurityPolicy	master	frame-src 'self'; frame-ancestors 'self'; object-src 'none';
_browser_header.xXSSProtection	master	1; mode=block
_browser_header.strictTransportSecurity	master	max-age=31536000; includeSubDomains
bruteForceProtected	master	false
permanentLockout	master	false
maxFailureWaitSeconds	master	900
minimumQuickLoginWaitSeconds	master	60
waitIncrementSeconds	master	60
quickLoginCheckMilliSeconds	master	1000
maxDeltaTimeSeconds	master	43200
failureFactor	master	30
displayName	master	Keycloak
displayNameHtml	master	<div class="kc-logo-text"><span>Keycloak</span></div>
defaultSignatureAlgorithm	master	RS256
offlineSessionMaxLifespanEnabled	master	false
offlineSessionMaxLifespan	master	5184000
oauth2DeviceCodeLifespan	graphql-demo	600
oauth2DevicePollingInterval	graphql-demo	5
cibaBackchannelTokenDeliveryMode	graphql-demo	poll
cibaExpiresIn	graphql-demo	120
cibaInterval	graphql-demo	5
cibaAuthRequestedUserHint	graphql-demo	login_hint
clientSessionIdleTimeout	graphql-demo	0
clientSessionMaxLifespan	graphql-demo	0
clientOfflineSessionIdleTimeout	graphql-demo	0
clientOfflineSessionMaxLifespan	graphql-demo	0
bruteForceProtected	graphql-demo	false
permanentLockout	graphql-demo	false
maxFailureWaitSeconds	graphql-demo	900
minimumQuickLoginWaitSeconds	graphql-demo	60
waitIncrementSeconds	graphql-demo	60
quickLoginCheckMilliSeconds	graphql-demo	1000
maxDeltaTimeSeconds	graphql-demo	43200
failureFactor	graphql-demo	30
actionTokenGeneratedByAdminLifespan	graphql-demo	43200
actionTokenGeneratedByUserLifespan	graphql-demo	300
defaultSignatureAlgorithm	graphql-demo	RS256
offlineSessionMaxLifespanEnabled	graphql-demo	false
offlineSessionMaxLifespan	graphql-demo	5184000
webAuthnPolicyRpEntityName	graphql-demo	keycloak
webAuthnPolicySignatureAlgorithms	graphql-demo	ES256
webAuthnPolicyRpId	graphql-demo	
webAuthnPolicyAttestationConveyancePreference	graphql-demo	not specified
webAuthnPolicyAuthenticatorAttachment	graphql-demo	not specified
webAuthnPolicyRequireResidentKey	graphql-demo	not specified
webAuthnPolicyUserVerificationRequirement	graphql-demo	not specified
webAuthnPolicyCreateTimeout	graphql-demo	0
webAuthnPolicyAvoidSameAuthenticatorRegister	graphql-demo	false
webAuthnPolicyRpEntityNamePasswordless	graphql-demo	keycloak
webAuthnPolicySignatureAlgorithmsPasswordless	graphql-demo	ES256
webAuthnPolicyRpIdPasswordless	graphql-demo	
webAuthnPolicyAttestationConveyancePreferencePasswordless	graphql-demo	not specified
webAuthnPolicyAuthenticatorAttachmentPasswordless	graphql-demo	not specified
webAuthnPolicyRequireResidentKeyPasswordless	graphql-demo	not specified
webAuthnPolicyUserVerificationRequirementPasswordless	graphql-demo	not specified
webAuthnPolicyCreateTimeoutPasswordless	graphql-demo	0
webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless	graphql-demo	false
client-policies.profiles	graphql-demo	{"profiles":[]}
client-policies.policies	graphql-demo	{"policies":[]}
_browser_header.contentSecurityPolicyReportOnly	graphql-demo	
_browser_header.xContentTypeOptions	graphql-demo	nosniff
_browser_header.xRobotsTag	graphql-demo	none
_browser_header.xFrameOptions	graphql-demo	SAMEORIGIN
_browser_header.contentSecurityPolicy	graphql-demo	frame-src 'self'; frame-ancestors 'self'; object-src 'none';
_browser_header.xXSSProtection	graphql-demo	1; mode=block
_browser_header.strictTransportSecurity	graphql-demo	max-age=31536000; includeSubDomains
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
master	jboss-logging
graphql-demo	jboss-logging
\.


--
-- Data for Name: realm_localizations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_localizations (realm_id, locale, texts) FROM stdin;
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	master
password	password	t	t	graphql-demo
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.redirect_uris (client_id, value) FROM stdin;
a1d8e48d-cb7a-4c10-9c10-5afb953dc4b0	/realms/master/account/*
95089ef3-abe8-41ed-8139-2d06fe16544f	/realms/master/account/*
4866beda-e11d-43f5-b327-7be9abd38666	/admin/master/console/*
b369c12d-3d77-423a-891d-70a3164948cc	/realms/graphql-demo/account/*
30a27344-f11c-4837-926d-324134b7b6fe	/admin/graphql-demo/console/*
316254a7-7597-4eab-bfca-6e492adce061	/realms/graphql-demo/account/*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
7c59b02f-eacc-4a95-a26c-941e900220e2	VERIFY_EMAIL	Verify Email	master	t	f	VERIFY_EMAIL	50
01a7a281-b145-4a09-a6ed-cbebc7d4519e	UPDATE_PROFILE	Update Profile	master	t	f	UPDATE_PROFILE	40
3e7ae8fb-0eef-4b07-850c-8f1c9e1a51a9	CONFIGURE_TOTP	Configure OTP	master	t	f	CONFIGURE_TOTP	10
4b003b56-dcb1-452f-aca5-9467ccfdb60c	UPDATE_PASSWORD	Update Password	master	t	f	UPDATE_PASSWORD	30
ce83699f-a901-4345-9a5c-7e07a7bb3f0f	terms_and_conditions	Terms and Conditions	master	f	f	terms_and_conditions	20
d2a5daff-d218-4749-bb05-a0a72eb6067c	update_user_locale	Update User Locale	master	t	f	update_user_locale	1000
61728a85-a57b-4f82-b742-4ab44a1b4d2b	delete_account	Delete Account	master	f	f	delete_account	60
19d6939b-20d5-45c1-bfb5-45d63aa0a08c	VERIFY_EMAIL	Verify Email	graphql-demo	t	f	VERIFY_EMAIL	50
ff2d1b88-e0fc-407c-ab8c-522566949571	UPDATE_PROFILE	Update Profile	graphql-demo	t	f	UPDATE_PROFILE	40
34779d6b-8334-4851-861a-089efdfa58b2	CONFIGURE_TOTP	Configure OTP	graphql-demo	t	f	CONFIGURE_TOTP	10
15c51810-54cf-49df-9b73-c3c670084515	UPDATE_PASSWORD	Update Password	graphql-demo	t	f	UPDATE_PASSWORD	30
fdc00be4-68a5-4860-86a2-640a36eb7cbd	terms_and_conditions	Terms and Conditions	graphql-demo	f	f	terms_and_conditions	20
ad9e77e1-a64b-4a83-9dd3-7a6019253023	update_user_locale	Update User Locale	graphql-demo	t	f	update_user_locale	1000
b174c092-f351-4912-b897-95cf87131a1a	delete_account	Delete Account	graphql-demo	f	f	delete_account	60
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_uris (resource_id, value) FROM stdin;
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
95089ef3-abe8-41ed-8139-2d06fe16544f	81219ff6-ecd7-49f4-883d-d272f6bd7e2d
b369c12d-3d77-423a-891d-70a3164948cc	2f96ec64-a810-4b6b-a2ba-52eea332ebac
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
74a38b15-6b6e-49af-ab74-913187ff9c11	\N	5b5e4698-3dce-4f42-88c3-f1167018fe8e	f	t	\N	\N	\N	master	admin	1624193316756	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
14a34700-0eaa-4ccc-bd85-df5055bfa4d4	74a38b15-6b6e-49af-ab74-913187ff9c11
66ed2ccf-c6d5-4dd1-a9ba-83774a502dfe	74a38b15-6b6e-49af-ab74-913187ff9c11
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.web_origins (client_id, value) FROM stdin;
4866beda-e11d-43f5-b327-7be9abd38666	+
30a27344-f11c-4837-926d-324134b7b6fe	+
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: realm_localizations realm_localizations_pkey; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_localizations
    ADD CONSTRAINT realm_localizations_pkey PRIMARY KEY (realm_id, locale);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_att_by_name_value; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_att_by_name_value ON public.client_attributes USING btree (name, value);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_css_preload; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_css_preload ON public.offline_client_session USING btree (client_id, offline_flag);


--
-- Name: idx_offline_uss_by_user; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_by_user ON public.offline_user_session USING btree (user_id, realm_id, offline_flag);


--
-- Name: idx_offline_uss_by_usersess; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_by_usersess ON public.offline_user_session USING btree (realm_id, offline_flag, user_session_id);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_createdon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_offline_uss_preload; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_preload ON public.offline_user_session USING btree (offline_flag, created_on, user_session_id);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_attribute_name; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_attribute_name ON public.user_attribute USING btree (name, value);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

