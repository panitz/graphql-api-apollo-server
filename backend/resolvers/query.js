// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
module.exports = {
  // eslint-disable-next-line
  sessions: (parent, args, { dataSources }, info) => {
    return dataSources.sessionAPI.getSessions(args)
  },
  // eslint-disable-next-line
  sessionById: (parent, { id }, { dataSources }, info) => {
    return dataSources.sessionAPI.getSessionById(id)
  },
  // eslint-disable-next-line
  speakers: (parent, args, { dataSources }, info) => {
    return dataSources.speakerAPI.getSpeakers()
  },
  // eslint-disable-next-line
  speakerById: (parent, { id }, { dataSources }, info) => {
    return dataSources.speakerAPI.getSpeakerById(id)
  },
}
