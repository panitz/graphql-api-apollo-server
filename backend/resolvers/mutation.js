// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
module.exports = {
  // eslint-disable-next-line
  toggleFavoriteSession: (parent, { id }, { dataSources }, info) => {
    return dataSources.sessionAPI.toggleFavoriteSession(id)
  },
  // eslint-disable-next-line no-unused-vars
  addNewSession: (parent, args, { dataSources }, info) =>
    dataSources.sessionAPI.addSession(args),
}
