const { DataSource } = require('apollo-datasource')
const _ = require('lodash')
const sessions = require('../data/sessions.json')

class SessionAPI extends DataSource {
  // eslint-disable-next-line
  initialize(config) {}

  // eslint-disable-next-line class-methods-use-this
  getSessions(args) {
    return _.filter(sessions, args)
  }

  // eslint-disable-next-line class-methods-use-this
  getSessionById(id) {
    const session = _.filter(sessions, { id: parseInt(id, 10) })
    return session[0]
  }

  // eslint-disable-next-line class-methods-use-this
  toggleFavoriteSession(id) {
    const session = _.filter(sessions, { id: parseInt(id, 10) })
    session[0].favorite = !session[0].favorite
    return session[0]
  }

  // eslint-disable-next-line class-methods-use-this
  addSession(session) {
    console.log(session)
  }
}

module.exports = SessionAPI
