const express = require('express')
const { ApolloServer } = require('apollo-server-express')
const fs = require('fs')
const http = require('http')
const https = require('https')
const SessionAPI = require('./datasource/sessions')
const SpeakerAPI = require('./datasource/speakers')
const typeDefs = require('./schema')
const resolvers = require('./resolvers')

const dataSources = () => ({
  sessionAPI: new SessionAPI(),
  speakerAPI: new SpeakerAPI(),
})

const sslCert = process.env.SSL_CERT
const sslKey = process.env.SSL_KEY
const useSsl = sslCert && sslKey

async function startApolloServer() {
  const server = new ApolloServer({ typeDefs, resolvers, dataSources })
  await server.start()

  const app = express()
  server.applyMiddleware({ app })

  let httpServer
  if (useSsl) {
    httpServer = https.createServer(
      {
        key: fs.readFileSync(sslKey),
        cert: fs.readFileSync(sslCert),
      },
      app,
    )
  } else {
    httpServer = http.createServer(app)
  }

  const port = process.env.PORT || 4000
  await new Promise((resolve) => httpServer.listen({ port }, resolve))
  console.log(
    '🚀 Server ready at',
    `http${useSsl ? 's' : ''}://localhost:${port}${server.graphqlPath}`,
  )
  return { server, app }
}

startApolloServer().then(() => {
  console.log('Server started')
})
