#!/usr/bin/env bash

CERTS_DIR=dev-certs

rm -rf "$CERTS_DIR"
mkdir -p "$CERTS_DIR"
mkcert -key-file "${CERTS_DIR}/key.pem" -cert-file "${CERTS_DIR}/cert.pem" example.com "*.example.com" localhost 127.0.0.1
