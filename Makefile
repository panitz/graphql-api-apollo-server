include .env

init-dev-environment:
	./scripts/init-dev-environment.sh

up:
	docker-compose up -d

stop:
	docker-compose stop

rm: stop
	docker-compose rm -s -f -v
	rm -rf temp/data

db-dump: up
	docker-compose exec db bash -c 'PGPASSWORD=${POSTGRES_PASSWORD} PGUSER=${POSTGRES_USER} pg_dump -d keycloak_dev | sed "/^-- Dumped by pg_dump version .*/a \\\\n\\\\connect keycloak_dev" > /docker-entrypoint-initdb.d/003-restore-keycloak-database.sql'
