# Building a GraphQL API with Apollo Server

Mein Code zum Pluralsight Kurs ["Building a GraphQL API with Apollo Server"](https://app.pluralsight.com/library/courses/graphql-api-apollo-server/table-of-contents)

## Keycloak

```shell
make up
make stop
make rm
make db-dump
```

Keycloak-Benutzer:

| Benutzername | Passwort |
| ------------ | -------- |
| admin        | admin    |
